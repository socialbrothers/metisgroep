<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php get_template_part('partials/single-post/hero'); ?>

		<?php get_template_part('partials/single-post/content'); ?>

		<?php get_template_part('partials/single-post/related'); ?>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>
