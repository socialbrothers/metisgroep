<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
  <div class="e-search">
    <?php /*<label for="s" class="screen-reader-text"><?php _e('Search for:','bonestheme'); ?></label>*/?>
    <input type="search" id="s" name="s" value="" placeholder="Typ een zoekterm" />
    <button type="submit" id="searchsubmit" class="e-search__submit" ><?php get_template_part('dist/icons/search.svg'); ?></button>
  </div>
</form>
