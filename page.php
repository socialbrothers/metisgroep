<?php get_header();

      if (have_posts()) :
        while (have_posts()) : the_post();

          get_template_part('partials/landing/header');

          get_template_part('partials/repeatable-sections/loop');

        endwhile;
      endif;

      get_footer(); ?>
