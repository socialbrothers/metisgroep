$(document).ready(function () {
  MIT_singe.init();
});

var MIT_singe = {
  init: function () {
    this.caching();
  },
  caching: function () {
    this.$hero = document.querySelector("#post_single_img img");
    this.$content = document.getElementById("post_single_content");
    this.$inner = document.getElementById("post_single_inner");
    if (this.$hero != undefined) {
      var currentObj = this;
      $(window)
        .resize(function () {
          currentObj.setup();
        })
        .resize();
    }
  },
  setup: function () {
    var height = this.$hero.offsetHeight;
    var calc_top = height / 2;

    this.$content.style.paddingTop = height - calc_top + "px";
    this.$inner.style.paddingBottom = calc_top + "px";
    this.$hero.style.bottom = -calc_top + "px";
  },
};
