import "../sass/main.scss";

import Header from "./components/header.js";
new Header();

import ScrollTo from "./components/ScrollTo.js";
window.ScrollTo = new ScrollTo();

import CategoryCheck from "./components/category-check.js";
new CategoryCheck();

import Pagination from "./components/pagination.js";
new Pagination();

import PostHeight from "./components/PostHeight.js";
new PostHeight();

import SliderYearlyBudget from "./components/sliderYearlyBudget.js";
new SliderYearlyBudget();

$(document).ready(function () {
	var check_if_location = true;
	var prevent_scheme = false;

	$(".current-menu-item .sub-menu a").on("click", function (e) {
		var hash = $(this)[0].hash;
		console.log(hash);
		if (hash) {
			e.preventDefault();
			var target = document.querySelector(hash);
			$("html, body").animate(
				{
					scrollTop: target.offsetTop - 100,
				},
				1000
			);
		}
	});

	$(window)
		.resize(function () {
			$(".height-calc").each(function () {
				$(this).css({
					height: "auto",
				});
				if (window.innerWidth > 740) {
					var the_height = $(this)[0].offsetHeight;
					if (the_height % 150 != 0) {
						var calc_height = Math.floor(the_height / 150) * 150;
						$(this).css({
							height: calc_height,
						});
					}
				}
			});

			$(".height-calc-client").each(function () {
				$(this).css({
					height: "auto",
				});
				if (window.innerWidth > 740) {
					var the_height = $(this)[0].offsetHeight;
					if (the_height % 150 != 0) {
						if (the_height / 150 < 2.2) {
							var calc_height = Math.ceil(the_height / 150) * 150;
						} else {
							var calc_height = Math.floor(the_height / 150) * 150;
						}

						$(this).css({
							height: calc_height,
						});
					}
				}
			});

			$(".height-calc-plus").each(function () {
				$(this).css({
					height: "auto",
				});
				if (window.innerWidth > 740) {
					var the_height = $(this)[0].offsetHeight;
					if (the_height % 150 != 0) {
						var calc_height = Math.ceil(the_height / 150) * 150;
						$(this).css({
							height: calc_height,
						});
					}
				}
			});

			if (window.location.href.indexOf("#") > -1 && check_if_location) {
				check_if_location = false;
				var current = window.scrollY;
				var to = current - 100;
				window.scrollTo(0, 0);
				$("html, body").animate(
					{
						scrollTop: to,
					},
					1000
				);
			}

			if (
				$(".c-roadmap__desktop").length > 0 &&
				window.innerWidth < 740 &&
				prevent_scheme == false
			) {
				prevent_scheme = true;
				MITdienstenMobile.init();
			}
		})
		.resize();
});

var MITdienstenMobile = {
	init: function () {
		this.$el = $(".c-roadmap__desktop");
		this.setup();
		this.getContent();
		this.eventListeners();
		new ScrollTo();
	},

	setup: function () {
		this.$mob_el = $(".c-roadmap__mobile");
		this.$button = $(".c-roadmap__mobile--row--header");
	},

	getContent: function () {
		var co = this;
		this.$rows = this.$el.find("tr");
		this.$rows.each(function (i) {
			var check = i == 0 ? true : false;
			$(this)
				.find("td")
				.each(function (i) {
					var spans = Number($(this).attr("colspan"));
					var content = $(this)[0].innerHTML;
					if (content == "&nbsp;") {
						return;
					} else if (check && i == 2) {
						co.postContent(5, spans, content);
					} else {
						co.postContent(i + 1, spans, content);
					}
				});
		});
	},

	postContent: function (col, limit, content) {
		this.$mob_el
			.find(".c-roadmap__mobile--row:nth-child(" + col + ")")
			.append(content);
	},

	eventListeners: function () {
		var co = this;
		window.ScrollTo.caching();
		this.$button.on("click", function () {
			co.$mob_el.find(".active").removeClass("active");
			$(this).parent().addClass("active");
		});
	},
};
