export default class Pagination {
  constructor() {
    this.$element = document.getElementById('mit_page_nav');
    if ( this.$element != undefined ) {
      this.setup();
    }
  }
  setup() {
    this.$current = this.$element.getElementsByClassName('current')[0];
    this.$next = this.$element.getElementsByClassName('next')[0];
    this.$prev = this.$element.getElementsByClassName('prev')[0];
    this.$current.parentNode.classList.add('current');
    if ( this.$next != undefined ) {
      this.$next.parentNode.classList.add('next');
    }
    if ( this.$prev != undefined ) {
      this.$prev.parentNode.classList.add('prev');
    }
  }
}
