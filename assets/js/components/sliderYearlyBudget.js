export default class SliderYearlyBudget {

  constructor() {
    const $ranges = document.getElementsByClassName('slider-yb__range');

    if ($ranges.length > 0) {
      this.eventListeners($ranges);
      this.setUp($ranges);
    }
  }

  eventListeners($ranges) {
    for (let i = 0; i < $ranges.length; i++) {
      const Obj = this;

      $ranges[i].addEventListener('change', function(e) {
        const $elChanged = e.srcElement;
        const value = Number($elChanged.value);

        Obj.changeState($elChanged, value);
        Obj.updateTotals($elChanged, value);
        Obj.removeAnimation();
      }, null);

    }
  }

  setUp($ranges) {
    for (let i = 0; i < $ranges.length; i++) {
      const $element = $ranges[i].getElementsByTagName('input')[0];
      const value = Number($element.value);

      this.changeState($element, value);
      this.updateTotals($element, value);
    }
  }

  changeState($element, value) {
    const $parent = $element.parentNode;
    const $currentActive = $parent.getElementsByClassName('active')[0];
    const $icons = $parent.getElementsByClassName('slider-yb__range--icon');

    value--;
    $currentActive.classList.remove('active');
    $icons[value].classList.add('active');
  }

  updateTotals($element, value) {
    const target = document.getElementById($element.dataset.target);
    const impact = Number($element.dataset.impact);
    const calc = (value - 1) * impact;

    target.style.width = calc+'%';
  }

  removeAnimation() {
    const element = document.querySelector('.c-rs-slider-yearly-budget__element');

    if (element.classList.contains('animation')) {
      element.classList.remove('animation');
    }
  }
}
