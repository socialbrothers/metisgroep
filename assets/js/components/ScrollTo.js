export default class ScrollTo {
  constructor(){
    this.caching();
  }
  caching() {
    this.$scroll_link = document.getElementsByClassName('js-scroll-to');
    if (this.$scroll_link.length > 0 ) {
      this.setup();
    }
  }
  setup(){
    for( let i = 0; i < this.$scroll_link.length; i++ ) {
      let currentObj = this;
      this.$scroll_link[i].addEventListener('click', function(e){
        e.preventDefault();
        currentObj.scroll(this);
      });
    }
  }
  scroll($el){
    let target = document.querySelector($el.hash);
    $('html, body').animate({
      scrollTop: target.offsetTop - 200
    }, 1000);
  }
}
