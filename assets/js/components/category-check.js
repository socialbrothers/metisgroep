export default class CategoryCheck {
  constructor() {
    this.$element = document.getElementById('category_check');
    if (this.$element != undefined) {
      this.setup();
      if ( document.getElementsByTagName('html')[0].classList.contains('touch') ) {
        let currentObj = this;
        this.$element.addEventListener('click', function() {
          if (currentObj.$element.classList.contains('open')) {
            currentObj.$element.classList.remove('open');
          } else {
            currentObj.$element.classList.add('open');
          }
        });
      }
    }
  }
  setup() {
    let active = this.$element.getElementsByClassName('current-cat')[0];
    if ( active != undefined ) {
      let text = active.childNodes[0].innerHTML;
      this.$element.getElementsByClassName('e-select__placeholder')[0].innerHTML = text;
    }
  }
}
