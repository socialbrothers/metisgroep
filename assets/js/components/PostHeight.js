export default class Header {
  constructor() {
    this.$posts = document.getElementsByClassName('c-post');
    this.posts_on_page = this.$posts.length;

    if (this.posts_on_page > 0) {
      if (window.innerWidth > 1000) {
        this.resize(2);
      } else if (window.innerWidth > 740 && window.innerWidth < 1000) {
        this.resize(1);
      }
    }
  }

  resize($interval){
    this.max_height = 0;

    this.count = 0;
    this.posts_array = [];

    for(let i = 0; i < this.posts_on_page; i++) {
      let current_height = this.$posts[i].offsetHeight;

      if ( current_height > this.max_height ) {
        this.max_height = current_height;
      }

      this.count++;
      this.posts_array.push(this.$posts[i]);

      if ( this.posts_on_page > $interval && this.count == ($interval + 1) ) {
        this.setup();
        this.emptyVars();
      }
    }

    if ( this.posts_on_page <= $interval ) {
      this.setup();
    }
  }
  setup(){
    for(let i = 0; i < this.posts_array.length; i++) {
      this.posts_array[i].style.height = this.max_height + 'px';
    }
  }
  emptyVars(){
    this.max_height = 0;
    this.count = 0;
    this.posts_array = [];
  }
}
