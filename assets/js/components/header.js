export default class Header {
  constructor() {
    this.caching();
    this.setup();
    this.eventListeners();
  }
  caching() {
    this.$header = document.getElementsByClassName('c-header')[0];
    this.$placeholder = document.getElementsByClassName('c-header--placeholder')[0];
    this.$button = this.$header.getElementsByClassName('c-header__indicator')[0];
  }
  setup() {
    if(window.innerWidth <= 480){
      let $menu = document.getElementById('menu-hoofdmenu');
      let li = document.createElement('li');
      li.id = 'contact-link';
      let a = document.createElement('a');
      a.href = '/contact';
      a.innerHTML = 'Contact';
      li.appendChild(a);
      $menu.appendChild(li);
    }
  }
  eventListeners() {
    let currentObj = this;
    let $html = document.getElementsByTagName('HTML')[0];
    $(window).resize(function() {
      if(window.innerWidth > 480){
        if ( document.getElementById('contact-link') != null ) {
          document.getElementById('contact-link').remove();
        }
      } else if(window.innerWidth <= 480 && document.getElementById('contact-link') == undefined ) {
        let $menu = document.getElementById('menu-hoofdmenu');
        let li = document.createElement('li');
        li.id = 'contact-link';
        let a = document.createElement('a');
        a.href = '/contact';
        a.innerHTML = 'Contact';
        li.appendChild(a);
        $menu.appendChild(li);
      }
    });
    this.$button.addEventListener('click', function() {
      if (currentObj.$header.classList.contains('c-header--animate-menu')) {
        currentObj.removeClass();
      } else {
        currentObj.addClass();
      }
    });
    this.$header.addEventListener('mouseenter', function() {
      currentObj.addClass();
    });
    this.$header.addEventListener('mouseleave', function() {
      currentObj.removeClass();
    });
  }
  addClass() {
    this.$header.className = 'c-header c-header--animate-menu';
  }
  removeClass() {
    this.$header.className = 'c-header';
  }
};
