			<footer class="c-footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<?php
				global $wp;
				$current_url = home_url(add_query_arg(array(), $wp->request));
				?>

				<div class="l-container">

					<div class="c-footer__share">
						<h4 class="e-heading"><?php the_field('mit_footer_share_title', 'option'); ?></h4>
						<div class="e-button__holder">
							<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $current_url; ?>" class="e-button e-button--icon e-button--grey"><?php get_template_part('dist/icons/linked-in.svg'); ?></a>
							<a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo $current_url; ?>" class="e-button e-button--icon e-button--grey"><?php get_template_part('dist/icons/facebook.svg'); ?></a>
							<a target="_blank" href="https://twitter.com/share" data-size="large" data-url="https://dev.twitter.com/web/tweet-button" data-via="twitterdev" data-related="twitterapi,twitter" class="e-button e-button--icon e-button--grey"><?php get_template_part('dist/icons/twitter.svg'); ?></a>
							<a href="mailto:?subject=<?php bloginfo('name'); ?>&body=<?php echo $current_url; ?>" class="e-button e-button--icon e-button--grey"><?php get_template_part('dist/icons/mail.svg'); ?></a>
							<a id="whatsapp-link" href="whatsapp://send?text=<?php echo $current_url; ?>" class="e-button e-button--icon e-button--grey"><?php get_template_part('dist/icons/whatsapp.svg'); ?></a>
						</div>
						<p class="footerbottom">
							<?php if (get_field('footer_certification_text', 'option')) : ?><?php the_field('footer_certification_text', 'option'); ?></br><?php endif; ?>
						<?php if (get_field('mit_footer_algemene_voorwaarden', 'option')) : ?><a target="_blank" title="Algemene voorwaarden" href="<?php the_field('mit_footer_algemene_voorwaarden', 'option'); ?>">Algemene voorwaarden</a><?php endif; ?>
						<?php
						$footerLinks = get_field('footer-links', 'option');
						if (!empty($footerLinks)) {
							foreach ($footerLinks as $link) {
								if (!empty($link['link']['url'])) {
									echo '<br /><a href="' . $link['link']['url'] . '" target="' . $link['link']['target'] . '">' . $link['link']['title'] . '</a>';
								}
							}
						}
						?>
						</p>
					</div>

					<div class="c-footer__contact">
						<h4 class="e-heading"><?php the_field('mit_footer_contact_title', 'option'); ?></h4>
						<div class="e-button__holder">
							<a href="<?php echo esc_url(get_page_link(5627)); ?>" class="e-button e-button--blue" style="min-width: 140px;">Contact</a>
						</div>
						<div class="e-mail__holder">
							<a class="e-icon e--icon-button e-icon--blue" href="mailto:<?php the_field('mit_email_metis', 'option'); ?>"><?php get_template_part('dist/icons/mail.svg'); ?> <?php the_field('mit_emaillabel_metis_it', 'option'); ?></a>
							<a class="e-icon e--icon-button e-icon--blue" href="mailto:<?php the_field('mit_email_metis_network', 'option'); ?>"><?php get_template_part('dist/icons/mail.svg'); ?> <?php the_field('mit_emaillabel_metis_network', 'option'); ?></a>
						</div>
						<p class="e-paragraph">
							<?php the_field('mit_footer_contact_address', 'option'); ?>
						</p>
					</div>

					<div class="c-footer__media">
						<h4 class="e-heading"><?php the_field('mit_footer_social_media_title', 'option'); ?></h4>
						<div class="e-button__holder">
							<?php if (get_field('mit_linked_in_url', 'option')) : ?><a class="e-button e-button--icon e-button--blue" title="LinkedIn Metis IT" target="_blank" href="<?php the_field('mit_linked_in_url', 'option'); ?>"><?php get_template_part('dist/icons/linked-in.svg'); ?></a><?php endif; ?>
							<?php if (get_field('mit_linkedin_network_url', 'option')) : ?><a class="e-button e-button--icon e-button--blue" title="LinkedIn Metis Network" target="_blank" href="<?php the_field('mit_linkedin_network_url', 'option'); ?>"><?php get_template_part('dist/icons/linked-in.svg'); ?></a><?php endif; ?>
							<?php if (get_field('mit_facebook_url', 'option')) : ?><a class="e-button e-button--icon e-button--blue" target="_blank" href="<?php the_field('mit_facebook_url', 'option'); ?>"><?php get_template_part('dist/icons/facebook.svg'); ?></a><?php endif; ?>
							<?php if (get_field('mit_twitter_url', 'option')) : ?><a class="e-button e-button--icon e-button--blue" target="_blank" href="<?php the_field('mit_twitter_url', 'option'); ?>"><?php get_template_part('dist/icons/twitter.svg'); ?></a><?php endif; ?>
						</div>
					</div>

					<?php /*<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.</p>*/ ?>

				</div>

			</footer>

			</div>

			<?php // all js scripts are loaded in library/bones.php 
			?>
			<?php // Google Analytics 
			?>
			<script>
				(function(i, s, o, g, r, a, m) {
					i['GoogleAnalyticsObject'] = r;
					i[r] = i[r] || function() {
						(i[r].q = i[r].q || []).push(arguments)
					}, i[r].l = 1 * new Date();
					a = s.createElement(o),
						m = s.getElementsByTagName(o)[0];
					a.async = 1;
					a.src = g;
					m.parentNode.insertBefore(a, m)
				})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

				ga('create', 'UA-73265032-1', 'auto');
				ga('send', 'pageview');
			</script>

			<?php if (is_page(6968)) : ?>
				<!-- Begin INDEED conversion code -->
				<script type="text/javascript">
					/* <![CDATA[ */
					var indeed_conversion_id = '6618892254448601';
					var indeed_conversion_label = '';
					/* ]]> */
				</script>
				<script type="text/javascript" src="//conv.indeed.com/applyconversion.js">
				</script>
				<noscript>
					<img height=1 width=1 border=0 src="//conv.indeed.com/pagead/conv/6618892254448601/?script=0">
				</noscript>
				<!-- End INDEED conversion code -->
			<?php endif; ?>

			<?php wp_footer(); ?>

			</body>

			</html> <!-- end of site. what a ride! -->