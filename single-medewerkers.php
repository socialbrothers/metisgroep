<?php get_header();

      if (have_posts()) :
        while (have_posts()) : the_post();

          get_template_part('partials/colleague/hero');

          get_template_part('partials/colleague/quote');

          get_template_part('partials/colleague/loop-articles');

          get_template_part('partials/colleague/team-filtered');

        endwhile;
      endif;

      get_footer(); ?>
