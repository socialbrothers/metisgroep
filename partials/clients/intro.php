<section class="c-page-intro clients height-calc-plus">

  <div class="l-container">
    <div class="l-col-6 c-page-intro__content">
      <h1 class="e-heading e-heading--1"><?php the_field('mit_clients_page_title', 'option'); ?></h1>
      <?php the_field('mit_clients_page_body', 'option'); ?>
    </div>
  </div>

</section>
