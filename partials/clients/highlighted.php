<section class="c-page-section grey">

  <div class="l-container">

    <div class="l-col">

      <?php while (have_posts()) : the_post(); ?>

        <?php get_template_part('partials/clients/client'); ?>

      <?php endwhile; ?>

    </div>
    
  </div>

</section>
