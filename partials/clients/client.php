<div class="c-client height-calc-client">

  <?php $logo = get_field('mit_client_logo'); ?>
  <div class="l-col-6">
    <div class="inner">

      <?php if (!empty($logo)): ?>
        <?php
          $url = $logo['url'];
        	$size = 'logo-2x';
          $thumb = $logo['sizes'][ $size ];
        ?>
        <img src="<?php echo $thumb; ?>" alt="<?php echo $logo['alt']; ?>" />
      <?php else: ?>
        <h2 class="e-heading e-heading--2"><?php the_title(); ?></h2>
      <?php endif; ?>

      <?php the_content(); ?>

    </div>
  </div>

  <?php $quote = get_field('mit_client_quote_body'); ?>
  <?php if ($quote) : ?>

    <div class="l-col-6">
      <p class="c-quote__quote"><?php echo $quote; ?></p>
      <?php if (get_field('mit_client_quote_from')): ?><p class="c-quote__from"><?php the_field('mit_client_quote_from'); ?>, </p><?php endif; ?>
      <p class="c-quote__function"><?php the_field('mit_client_quote_function'); ?></p>
    </div>

  <?php endif; ?>

</div>
