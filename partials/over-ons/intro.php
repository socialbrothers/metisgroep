<section id="intro" class="c-page-intro height-calc-plus about">

  <div class="l-container">
    <div class="l-col-6 c-page-intro__content">
      <h1 class="e-heading e-heading--1"><?php the_title(); ?></h1>
      <?php the_content(); ?>
    </div>
  </div>

</section>
