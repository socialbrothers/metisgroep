<?php

  /*
  * Loop arguments & query posts
  */



  if(isset($_GET['afdeling']) && $_GET['afdeling'] != ''):
    if($_GET['afdeling'] == 'metis_it'):
      $metis_afdeling = 'a';
    elseif($_GET['afdeling'] == 'metis_network'):
      $metis_afdeling = 'b';
    endif;

    $args = array(
      'post_type'      => 'medewerkers',
      'posts_per_page' => -1,
      'meta_key'       => 'achternaam',
      'orderby'        => 'meta_value',
      'order'          => 'ASC',
      'meta_query' => array(
        'relation' => 'OR',
        array(
            'key'     => 'metis_afdeling',
            'value'   => $metis_afdeling,
            'compare' => '=',
        ),
        array(
            'key'     => 'metis_afdeling',
            'value'   => 'c',
            'compare' => '=',
        ),
      ),
    );
  else :
    $args = array(
      'post_type'      => 'medewerkers',
      'posts_per_page' => -1,
      'meta_key'       => 'achternaam',
      'orderby'        => 'meta_value',
      'order'          => 'ASC'
    );
  endif;



  $the_query = new WP_Query( $args );

  /*
  * The loop (Nieuws)
  */
  if ( $the_query->have_posts() ) :
?>

    <div class="l-container">

      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <?php get_template_part('partials/shared/colleague'); ?>

      <?php
        endwhile;
        wp_reset_postdata();
      ?>

    </div>

<?php endif; ?>
