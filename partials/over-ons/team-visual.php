<section id="team" class="c-team-visual">

  <div class="c-team-visual__background">
    <div class="c-team-visual__image"></div>
    <?php get_template_part('dist/icons/home-team.svg'); ?>
    <div class="c-team-visual__background--top"></div>
    <div class="c-team-visual__background--bottom"></div>
  </div>

  <div class="c-team-visual__content">
    <div class="l-container">
      <div class="c-team-visual__left--about">
        <h1 class="e-heading e-heading--1">Team</h1>
      </div>
    </div>
  </div>

</section>
