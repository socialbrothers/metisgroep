<section id="werken-bij" class="c-page-section c-work-at height-calc-plus">

  <div class="l-container c-page-section__title">
    <div class="l-col">
      <h1 class="e-heading e-heading--1"><?php the_field('mit_about_us_work_here_title'); ?></h1>
    </div>
  </div>

  <div class="l-container">

    <div class="l-col-4">
      <div class="c-text-block transparent">
        <div class="c-text-block__inner">
          <?php the_field('mit_about_us_work_here__body_column_one'); ?>
        </div>
        <div class="c-text-block__background"></div>
      </div>
    </div>

    <?php if ( get_field('mit_about_us_work_here_vacature_hide') != 1 ) : ?>
      <div class="l-col-4">
        <div class="c-text-block c-vac-link">
          <a class="c-vac-link__url" href="<?php the_field('mit_about_us_work_here_vacature_link_url'); ?>"><?php the_field('mit_about_us_work_here_vacature_link_text'); ?></a>
          <div class="c-text-block__inner">
            <h2 class="e-heading e-heading--2"><?php the_field('mit_about_us_work_here_vacature_title'); ?></h2>
            <div class="c-vac-link__text">
              <?php the_field('mit_about_us_work_here_vacature_link_text'); ?>
            </div>
          </div>
          <?php
            $image = get_field('mit_about_us_work_here_vacature_image');
          ?>
          <div class="c-text-block__background" <?php if( !empty($image) ): ?>style="background-image: url('<?php echo $image['url']; ?>');"<?php endif; ?>></div>
        </div>
      </div>
    <?php endif; ?>

    <div class="l-col-4">
      <div class="c-text-block white">
        <div class="c-text-block__inner">
          <?php the_field('mit_about_us_work_here__body_column_two'); ?>
        </div>
        <div class="c-text-block__background"></div>
      </div>
    </div>

    <div class="l-col-4">
      <div class="c-text-block white">
        <div class="c-text-block__inner">
          <?php the_field('mit_about_us_work_here__body_column_three'); ?>
        </div>
        <div class="c-text-block__background"></div>
      </div>
    </div>

    <?php if ( get_field('mit_about_us_work_here_vacature_two_hide') != 1 ) : ?>
      <div class="l-col-4">
        <div class="c-text-block c-vac-link">
          <a class="c-vac-link__url" href="<?php the_field('mit_about_us_work_here_vacature_two_link_url'); ?>"><?php the_field('mit_about_us_work_here_vacature_two_link_text'); ?></a>
          <div class="c-text-block__inner">
            <h2 class="e-heading e-heading--2"><?php the_field('mit_about_us_work_here_vacature_two_title'); ?></h2>
            <div class="c-vac-link__text">
              <?php the_field('mit_about_us_work_here_vacature_two_link_text'); ?>
            </div>
          </div>
          <?php
            $image = get_field('mit_about_us_work_here_vacature_two_image');
          ?>
          <div class="c-text-block__background" <?php if( !empty($image) ): ?>style="background-image: url('<?php echo $image['url']; ?>');"<?php endif; ?>></div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ( get_field('mit_about_us_work_here_vacature_three_hide') != 1 ) : ?>
      <div class="l-col-4">
        <div class="c-text-block c-vac-link">
          <a class="c-vac-link__url" href="<?php the_field('mit_about_us_work_here_vacature_three_link_url'); ?>"><?php the_field('mit_about_us_work_here_vacature_three_link_text'); ?></a>
          <div class="c-text-block__inner">
            <h2 class="e-heading e-heading--2"><?php the_field('mit_about_us_work_here_vacature_three_title'); ?></h2>
            <div class="c-vac-link__text">
              <?php the_field('mit_about_us_work_here_vacature_three_link_text'); ?>
            </div>
          </div>
          <?php
            $image = get_field('mit_about_us_work_here_vacature_three_image');
          ?>
          <div class="c-text-block__background" <?php if( !empty($image) ): ?>style="background-image: url('<?php echo $image['url']; ?>');"<?php endif; ?>></div>
        </div>
      </div>
    <?php endif; ?>

  </div>

</section>
