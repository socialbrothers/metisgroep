<section id="kernkwaliteiten" class="c-page-section padding-eq c-core-qualities height-calc">
  <div class="l-container">
    <div class="l-col-6 c-core-qualities__content">
      <h1 class="e-heading e-heading--1"><?php the_field('mit_about_us_core_qualities_title'); ?></h1>
      <?php the_field('mit_about_us_core_qualities_body');?>
    </div>
    <div class="l-col-6 c-core-qualities__img">
      <?php get_template_part('dist/icons/over-ons-core-quality-icon.svg'); ?>
    </div>
  </div>
</section>
