<?php
$title = get_sub_field('title');
$text = get_sub_field('text');
?>
<section class="faq-items">
  <div class="faq-content">
    <button class="collapsible"><h3><?=($title)?$title:' - ';?></h3></button>
    <?=($text)?'<div class="content_container"><div class="content">'.$text.'</div></div>':'';?>
  </div>
</section>