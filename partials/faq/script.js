/**
 * JS for FAQ item
 *
 * @TODO SHOULD be migrated to SCSS and JS compiler (webpack).
 * @TODO But current package.json has errors webpack cannot be installed.
 */

jQuery( document ).ready(function() {
  var coll = document.getElementsByClassName("collapsible");
  var i;
  for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var content = this.nextElementSibling;
      if (content.style.maxHeight){
        content.style.maxHeight = null;
      } else {
        content.style.maxHeight = content.scrollHeight + "px";
      }
    });
  }
});