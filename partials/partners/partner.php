<div class="c-client">

  <?php $logo = get_field('mit_partner_logo'); ?>
  <div class="l-col-6">
    <div class="inner">

      <?php if (!empty($logo)): ?>
        <?php
          $url = $logo['url'];
        	$size = 'logo-2x';
          $thumb = $logo['sizes'][ $size ];
        ?>
        <?php echo ( get_field('mit_partner_url') ) ? '<a href="'.get_field('mit_partner_url').'" target="_blank">' : '' ; ?>
          <img src="<?php echo $thumb; ?>" alt="<?php echo $logo['alt']; ?>" />
        <?php echo ( get_field('mit_partner_url') ) ? '</a>' : '' ; ?>
      <?php else: ?>
        <?php echo ( get_field('mit_partner_url') ) ? '<a href="'.get_field('mit_partner_url').'" target="_blank">' : '' ; ?>
          <h2 class="e-heading e-heading--2"><?php the_title(); ?></h2>
        <?php echo ( get_field('mit_partner_url') ) ? '</a>' : '' ; ?>
      <?php endif; ?>

      <?php the_content(); ?>

    </div>
  </div>

  <div class="l-col-6">
    <?php $quote = get_field('mit_partner_quote_body'); ?>
    <?php if ($quote) : ?>
      <p class="c-quote__quote"><?php echo $quote; ?></p>
      <p class="c-quote__from"><?php the_field('mit_partner_quote_from'); ?></p>
      <p class="c-quote__function"><?php the_field('mit_partner_quote_function'); ?></p>
    <?php endif; ?>
  </div>

</div>
