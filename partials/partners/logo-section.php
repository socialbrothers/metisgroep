<?php if (have_rows('partners-settings-logos-repeater', 'option')) : ?>

  <section id="overzicht" class="c-page-section">

    <div class="l-container">

      <?php while (have_rows('partners-settings-logos-repeater', 'option')) : the_row(); ?>

        <?php
        $image = get_sub_field('partners-settings-logos-image', 'option') ?? [];
        $logo_link = get_sub_field('partners-settings-logos-link', 'option') ?? [];
        ?>

        <?php if (!empty($image['ID'])) : ?>

          <div class="l-col-3 c-logo">
            <a href="<?= $logo_link['url'] ?? '' ?>" target="_blank">
              <?= wp_get_attachment_image($image['ID'], 'full', true); ?>
            </a>
          </div>

        <?php endif; ?>

      <?php endwhile; ?>

    </div>

  </section>

<?php endif; ?>