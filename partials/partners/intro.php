<section class="c-page-intro height-calc-plus clients">

  <div class="l-container">
    <div class="l-col-6 c-page-intro__content">
      <h1 class="e-heading e-heading--1"><?php the_field('mit_partners_page_title', 'option'); ?></h1>
      <?php the_field('mit_partners_page_body', 'option'); ?>
    </div>
  </div>

</section>