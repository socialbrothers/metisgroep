<section id="associate-network" class="c-page-section">

  <div class="l-container">

    <div class="l-col">

      <h1 class="e-heading e-heading--1" style="margin-bottom: 31px;"><?php the_field('mit_partners_associate_network_title', 'option'); ?></h1>

      <?php the_field('mit_partners_associate_network_body', 'option'); ?>

    </div>

  </div>

</section>
