<?php $slug = get_post_field( 'post_name', get_post() ); ?>

<section id="<?php echo $slug; ?>" class="c-rs-puzzle">

  <div class="l-container c-page-section__title">

    <div class="l-col">

      <h1 class="e-heading e-heading--1"><?php the_title(); ?></h1>

    </div>

  </div>

  <div class="l-container">

    <?php the_sub_field('mit_rs_section_type_puzzle_body_top'); ?>

    <div class="c-rs-puzzle__holder">
      <svg width="1103" height="433" viewBox="0 0 1103 433" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><g fill="#D7D7D7"><path d="M563.32 275.3l143.54-143.456H419.78z"/><path d="M419.78 131.844L276.24 275.301h287.08zM814.637 167.81l72.015 72.015V95.795zM521.624 30L419.78 131.844h101.845zM563.32 275.3l71.81 71.815 71.808-71.815-71.808-71.809z"/><path d="M778.711 203.776l35.926-35.965 35.926-35.966H706.86l-71.85 71.93zM216.5 131.845l101.64 101.64 101.639-101.641z"/></g><path fill="#7F268E" d="M960 173.5L1103 317V30z"/><path fill="#581797" d="M828.5 247L685 390h287z"/><path fill="#F65275" d="M360 361l72 72V289z"/><path fill="#A73486" d="M223 289L121 391h102zM0 245.997L72 318l72-72.003L72 174z"/><path fill="#CE437D" d="M176 102l36-36 36-36H104l-72 72z"/><path fill="#30089F" d="M669 .001L770.5 102 872 0z"/></g></svg>
    </div>

    <?php the_sub_field('mit_rs_section_type_puzzle_body_bottom'); ?>

  </div>

</section>
