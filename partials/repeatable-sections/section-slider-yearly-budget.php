<?php $slug = get_post_field( 'post_name', get_post() ); ?>

<section id="<?php echo $slug; ?>" class="c-rs-slider-yearly-budget">

  <div class="l-container c-page-section__title">

    <div class="l-col">

      <h1 class="e-heading e-heading--1"><?php the_title(); ?></h1>

    </div>

  </div>

  <div class="l-container">

    <div class="l-col-4 c-rs-slider-yearly-budget__text">

      <?php the_content(); ?>

    </div>

    <div class="l-col-8 c-rs-slider-yearly-budget__element animation">

      <div class="l-container slider-yb">

        <div class="l-container">
          <h2>Deel zelf je jaarbudget in</h2>

          <div class="slider-yb__totals">
            <div id="sliderRangeVervoer" class="slider-yb__totals--el"></div>
            <div id="sliderRangeMiddelen" class="slider-yb__totals--el"></div>
            <div id="sliderRangeVakantie" class="slider-yb__totals--el"></div>
          </div>

          <div class="slider-yb__totals--text light">
            Besteed
          </div>
          <div class="slider-yb__totals--text dark">
            Salaris
          </div>
        </div>


        <div class="l-col-4">

          <div class="slider-yb__heading">Vervoer</div>
          <div class="slider-yb__heading">Middelen</div>
          <div class="slider-yb__heading">Vakantie</div>

        </div><!-- /.l-col-4 -->

        <div class="l-col-8">

          <div>

            <div class="slider-yb__range">
              <input data-target="sliderRangeVervoer" data-impact="10" type="range" min="1" max="4" step="1">
              <div class="slider-yb__range--track">
                <div class="slider-yb__range--icon"></div>
                <div class="slider-yb__range--icon"></div>
                <div class="slider-yb__range--icon active"></div>
                <div class="slider-yb__range--icon"></div>
              </div>
            </div>

            <div class="slider-yb__range">
              <input data-target="sliderRangeMiddelen" data-impact="4" type="range" min="1" max="4" step="1">
              <div class="slider-yb__range--track">
                <div class="slider-yb__range--icon"></div>
                <div class="slider-yb__range--icon"></div>
                <div class="slider-yb__range--icon active"></div>
                <div class="slider-yb__range--icon"></div>
              </div>
            </div>

            <div class="slider-yb__range">
              <input data-target="sliderRangeVakantie" data-impact="3" type="range" min="1" max="4" step="1">
              <div class="slider-yb__range--track">
                <div class="slider-yb__range--icon"></div>
                <div class="slider-yb__range--icon"></div>
                <div class="slider-yb__range--icon active"></div>
                <div class="slider-yb__range--icon"></div>
              </div>
            </div>

          </div>

        </div><!-- /.l-col-8 -->

      </div>

    </div>

  </div>

</section>
