<?php $slug = get_post_field( 'post_name', get_post() ); ?>

<section id="<?php echo $slug; ?>" class="c-rs-vacancies">

  <div class="l-container c-page-section__title">

    <div class="l-col">

      <h1 class="e-heading e-heading--1"><?php the_title(); ?></h1>

    </div>

  </div>

  <div class="l-container">

    <?php /* $pageAboutUsVacanciesId = 5185; */?>
    <?php if ( get_field('mit_about_us_work_here_vacature_hide') != 1 ) : ?>
      <div class="l-col-4">
        <div class="c-text-block c-vac-link">
          <a class="c-vac-link__url" href="<?php echo home_url(); ?><?php the_field('mit_about_us_work_here_vacature_link_url'); ?>"><?php the_field('mit_about_us_work_here_vacature_link_text'); ?></a>
          <div class="c-text-block__inner">
            <h2 class="e-heading e-heading--2"><?php the_field('mit_about_us_work_here_vacature_title'); ?></h2>
            <div class="c-vac-link__text">
              <?php the_field('mit_about_us_work_here_vacature_link_text'); ?>
            </div>
          </div>
          <?php
            $image = get_field('mit_about_us_work_here_vacature_image');
          ?>
          <div class="c-text-block__background" <?php if( !empty($image) ): ?>style="background-image: url('<?php echo $image['url']; ?>');"<?php endif; ?>></div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ( get_field('mit_about_us_work_here_vacature_two_hide') != 1 ) : ?>
      <div class="l-col-4">
        <div class="c-text-block c-vac-link">
          <a class="c-vac-link__url" href="<?php echo home_url(); ?><?php the_field('mit_about_us_work_here_vacature_two_link_url'); ?>"><?php the_field('mit_about_us_work_here_vacature_two_link_text'); ?></a>
          <div class="c-text-block__inner">
            <h2 class="e-heading e-heading--2"><?php the_field('mit_about_us_work_here_vacature_two_title'); ?></h2>
            <div class="c-vac-link__text">
              <?php the_field('mit_about_us_work_here_vacature_two_link_text'); ?>
            </div>
          </div>
          <?php
            $image = get_field('mit_about_us_work_here_vacature_two_image');
          ?>
          <div class="c-text-block__background" <?php if( !empty($image) ): ?>style="background-image: url('<?php echo $image['url']; ?>');"<?php endif; ?>></div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ( get_field('mit_about_us_work_here_vacature_three_hide') != 1 ) : ?>
      <div class="l-col-4">
        <div class="c-text-block c-vac-link">
          <a class="c-vac-link__url" href="<?php echo home_url(); ?><?php the_field('mit_about_us_work_here_vacature_three_link_url'); ?>"><?php the_field('mit_about_us_work_here_vacature_three_link_text'); ?></a>
          <div class="c-text-block__inner">
            <h2 class="e-heading e-heading--2"><?php the_field('mit_about_us_work_here_vacature_three_title'); ?></h2>
            <div class="c-vac-link__text">
              <?php the_field('mit_about_us_work_here_vacature_three_link_text'); ?>
            </div>
          </div>
          <?php
            $image = get_field('mit_about_us_work_here_vacature_three_image');
          ?>
          <div class="c-text-block__background" <?php if( !empty($image) ): ?>style="background-image: url('<?php echo $image['url']; ?>');"<?php endif; ?>></div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ( get_field('mit_about_us_work_here_vacature_four_hide') != 1 ) : ?>
      <div class="l-col-4">
        <div class="c-text-block c-vac-link">
          <a class="c-vac-link__url" href="<?php echo home_url(); ?><?php the_field('mit_about_us_work_here_vacature_four_link_url'); ?>"><?php the_field('mit_about_us_work_here_vacature_four_link_text'); ?></a>
          <div class="c-text-block__inner">
            <h2 class="e-heading e-heading--2"><?php the_field('mit_about_us_work_here_vacature_four_title'); ?></h2>
            <div class="c-vac-link__text">
              <?php the_field('mit_about_us_work_here_vacature_four_link_text'); ?>
            </div>
          </div>
          <?php
            $image = get_field('mit_about_us_work_here_vacature_four_image');
          ?>
          <div class="c-text-block__background" <?php if( !empty($image) ): ?>style="background-image: url('<?php echo $image['url']; ?>');"<?php endif; ?>></div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ( get_field('mit_about_us_work_here_vacature_five_hide') != 1 ) : ?>
      <div class="l-col-4">
        <div class="c-text-block c-vac-link">
          <a class="c-vac-link__url" href="<?php echo home_url(); ?><?php the_field('mit_about_us_work_here_vacature_five_link_url'); ?>"><?php the_field('mit_about_us_work_here_vacature_five_link_text'); ?></a>
          <div class="c-text-block__inner">
            <h2 class="e-heading e-heading--2"><?php the_field('mit_about_us_work_here_vacature_five_title'); ?></h2>
            <div class="c-vac-link__text">
              <?php the_field('mit_about_us_work_here_vacature_five_link_text'); ?>
            </div>
          </div>
          <?php
            $image = get_field('mit_about_us_work_here_vacature_five_image');
          ?>
          <div class="c-text-block__background" <?php if( !empty($image) ): ?>style="background-image: url('<?php echo $image['url']; ?>');"<?php endif; ?>></div>
        </div>
      </div>
    <?php endif; ?>


	  <?php if ( get_field('mit_about_us_work_here_vacature_six_hide') != 1 ) : ?>
      <div class="l-col-4">
        <div class="c-text-block c-vac-link">
          <a class="c-vac-link__url" href="<?php echo home_url(); ?><?php the_field('mit_about_us_work_here_vacature_six_link_url'); ?>"><?php the_field('mit_about_us_work_here_vacature_six_link_text'); ?></a>
          <div class="c-text-block__inner">
            <h2 class="e-heading e-heading--2"><?php the_field('mit_about_us_work_here_vacature_six_title'); ?></h2>
            <div class="c-vac-link__text">
              <?php the_field('mit_about_us_work_here_vacature_six_link_text'); ?>
            </div>
          </div>
          <?php
            $image = get_field('mit_about_us_work_here_vacature_six_image');
          ?>
          <div class="c-text-block__background" <?php if( !empty($image) ): ?>style="background-image: url('<?php echo $image['url']; ?>');"<?php endif; ?>></div>
        </div>
      </div>
	  <?php endif; ?>

	  <?php if ( get_field('mit_about_us_work_here_vacature_seven_hide') != 1 ) : ?>
      <div class="l-col-4">
        <div class="c-text-block c-vac-link">
          <a class="c-vac-link__url" href="<?php echo home_url(); ?><?php the_field('mit_about_us_work_here_vacature_seven_link_url'); ?>"><?php the_field('mit_about_us_work_here_vacature_seven_link_text'); ?></a>
          <div class="c-text-block__inner">
            <h2 class="e-heading e-heading--2"><?php the_field('mit_about_us_work_here_vacature_seven_title'); ?></h2>
            <div class="c-vac-link__text">
              <?php the_field('mit_about_us_work_here_vacature_seven_link_text'); ?>
            </div>
          </div>
          <?php
            $image = get_field('mit_about_us_work_here_vacature_seven_image');
          ?>
          <div class="c-text-block__background" <?php if( !empty($image) ): ?>style="background-image: url('<?php echo $image['url']; ?>');"<?php endif; ?>></div>
        </div>
      </div>
	  <?php endif; ?>

	  <?php if ( get_field('mit_about_us_work_here_vacature_eight_hide') != 1 ) : ?>
      <div class="l-col-4">
        <div class="c-text-block c-vac-link">
          <a class="c-vac-link__url" href="<?php echo home_url(); ?><?php the_field('mit_about_us_work_here_vacature_eight_link_url'); ?>"><?php the_field('mit_about_us_work_here_vacature_eight_link_text'); ?></a>
          <div class="c-text-block__inner">
            <h2 class="e-heading e-heading--2"><?php the_field('mit_about_us_work_here_vacature_eight_title'); ?></h2>
            <div class="c-vac-link__text">
              <?php the_field('mit_about_us_work_here_vacature_eight_link_text'); ?>
            </div>
          </div>
          <?php
            $image = get_field('mit_about_us_work_here_vacature_eight_image');
          ?>
          <div class="c-text-block__background" <?php if( !empty($image) ): ?>style="background-image: url('<?php echo $image['url']; ?>');"<?php endif; ?>></div>
        </div>
      </div>
	  <?php endif; ?>

	  <?php if ( get_field('mit_about_us_work_here_vacature_nine_hide') != 1 ) : ?>
      <div class="l-col-4">
        <div class="c-text-block c-vac-link">
          <a class="c-vac-link__url" href="<?php echo home_url(); ?><?php the_field('mit_about_us_work_here_vacature_nine_link_url'); ?>"><?php the_field('mit_about_us_work_here_vacature_nine_link_text'); ?></a>
          <div class="c-text-block__inner">
            <h2 class="e-heading e-heading--2"><?php the_field('mit_about_us_work_here_vacature_nine_title'); ?></h2>
            <div class="c-vac-link__text">
              <?php the_field('mit_about_us_work_here_vacature_nine_link_text'); ?>
            </div>
          </div>
          <?php
            $image = get_field('mit_about_us_work_here_vacature_nine_image');
          ?>
          <div class="c-text-block__background" <?php if( !empty($image) ): ?>style="background-image: url('<?php echo $image['url']; ?>');"<?php endif; ?>></div>
        </div>
      </div>
	  <?php endif; ?>

	  <?php if ( get_field('mit_about_us_work_here_vacature_ten_hide') != 1 ) : ?>
      <div class="l-col-4">
        <div class="c-text-block c-vac-link">
          <a class="c-vac-link__url" href="<?php echo home_url(); ?><?php the_field('mit_about_us_work_here_vacature_ten_link_url'); ?>"><?php the_field('mit_about_us_work_here_vacature_ten_link_text'); ?></a>
          <div class="c-text-block__inner">
            <h2 class="e-heading e-heading--2"><?php the_field('mit_about_us_work_here_vacature_ten_title'); ?></h2>
            <div class="c-vac-link__text">
              <?php the_field('mit_about_us_work_here_vacature_ten_link_text'); ?>
            </div>
          </div>
          <?php
            $image = get_field('mit_about_us_work_here_vacature_ten_image');
          ?>
          <div class="c-text-block__background" <?php if( !empty($image) ): ?>style="background-image: url('<?php echo $image['url']; ?>');"<?php endif; ?>></div>
        </div>
      </div>
	  <?php endif; ?>





  </div>

</section>
