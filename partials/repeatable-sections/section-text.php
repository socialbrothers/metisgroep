<?php $slug = get_post_field( 'post_name', get_post() ); ?>

<section id="<?php echo $slug; ?>" class="c-rs-text height-calc">

  <div class="l-container c-page-section__title">

    <div class="l-col">

      <h1 class="e-heading e-heading--1"><?php the_title(); ?></h1>

    </div>

  </div>

  <div class="l-container">

    <?php the_sub_field('mit_rs_section_type_text_body'); ?>

  </div>

</section>
