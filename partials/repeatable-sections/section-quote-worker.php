<?php
  $slug = get_post_field( 'post_name', get_post() );

  $workerID = get_sub_field('mit_rs_section_type_quote_worker_id');

  if ( !empty($workerID) && get_field('mit_colleage_quote_body', $workerID) ) :
?>

  <section id="<?php echo $slug; ?>" class="c-home-testimonials c-rs-quote-worker">

    <div class="l-container">

      <div class="c-home-testimonials__item">
        <p class="c-quote__quote"><?php the_field('mit_colleage_quote_body', $workerID); ?></p>
        <p class="c-quote__from"><?php echo get_the_title($workerID); ?></p>
        <p class="c-quote__function"><?php the_field('functie', $workerID); ?></p>
      </div>

    </div>

  </section>

<?php endif; ?>
