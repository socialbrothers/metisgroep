<?php // Check if page has repeatable sections ?>
<?php if( have_rows('mit_rs') ): ?>

  <?php // Loop through repeatable sections ?>
  <?php while ( have_rows('mit_rs') ) : the_row(); ?>

    <?php
      // Get the post object of the section
      $post_object = get_sub_field('mit_rs_section');

      if( $post_object ):
        // override $post
	      $post = $post_object;
	      setup_postdata( $post );

        // Check if post object has a custom section field
        if( have_rows('mit_rs_section_type') ):

          // Loop through custom section field
          while ( have_rows('mit_rs_section_type') ) : the_row();

            // Check the section type and load a template
            if( get_row_layout() == 'mit_rs_section_type_text' ):

              get_template_part('partials/repeatable-sections/section', 'text');

            elseif( get_row_layout() == 'mit_rs_section_type_vacancy' ):

              get_template_part('partials/repeatable-sections/section', 'vacancies');

            elseif( get_row_layout() == 'mit_rs_section_type_puzzle' ):

              get_template_part('partials/repeatable-sections/section', 'puzzle');

            elseif( get_row_layout() == 'mit_rs_section_type_quote_worker' ):

              get_template_part('partials/repeatable-sections/section', 'quote-worker');

            elseif( get_row_layout() == 'mit_rs_section_type_slider_yearly_budget' ):

              get_template_part('partials/repeatable-sections/section', 'slider-yearly-budget');

            endif;

          endwhile;

          wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly

        endif;

      ?>

      <?php  ?>

    <?php endif; ?>

  <?php endwhile; ?>

<?php endif; ?>
