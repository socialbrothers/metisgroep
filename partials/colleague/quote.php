<?php
  $client = get_field('mit_colleage_quote_from');

  if($client) :

    $client_id = $client->ID;
    $quote = get_field('mit_client_quote_body', $client_id);
    $quote_from = get_field('mit_client_quote_from', $client_id);
    $quote_function = get_field('mit_client_quote_function', $client_id);
?>

<section class="c-quote">

  <div class="l-container">

    <div class="l-col-6">

      <p class="c-quote__quote"><?php echo $quote; ?></p>
      <p class="c-quote__from"><?php echo $quote_from; ?></p>
      <p class="c-quote__function"><?php echo $quote_function; ?></p>

    </div>

  </div>

</section>

<?php endif; ?>
