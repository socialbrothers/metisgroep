<?php
  $user = get_field('toon_blogs_van');

  if ($user) :

  /*
  * Loop arguments & query posts
  */
  $args = array(
    'showposts' => 3,
    'post_type' => 'post',
    'author' => $user['ID'],
  );

  $the_query = new WP_Query( $args );

  /*
  * The loop (Nieuws)
  */
  if ( $the_query->have_posts() ) :
?>

  <section class="c-page-section grey padding-eq">

    <div class="l-container c-page-section__title">

      <div class="l-col">

        <h1 class="e-heading e-heading--1">Blogs van <?php the_field('voornaam'); ?></h1>

      </div>

    </div>

    <div class="l-container">

      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <?php get_template_part('partials/shared/post'); ?>

      <?php
        endwhile;
        wp_reset_postdata();
      ?>

      <?php if(get_the_author_posts($user['ID']) > 3) : ?>

        <div class="l-col">
          <a class="e-button e-button--pink" style="padding-right: 40px;" href="<?php echo get_author_posts_url( $user['ID'] ); ?> "><span>Bekijk alle blogs <?php the_field('voornaam'); ?></span></a>
        </div>
      <?php endif; ?>

    </div>

  </section>

<?php endif; endif; ?>
