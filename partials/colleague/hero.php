<section class="c-colleague-hero height-calc-plus">

  <div class="l-container">

    <div class="l-col-6 c-colleague-hero__title">
      <div class="c-post-single__category">Team</div>
      <h1 class="e-heading e-heading--1"><?php the_title(); ?></h1>
      <h2 class="e-heading e-heading--2"><?php the_field('functie'); ?></h2>
    </div>

  </div>

  <div class="l-container">

    <div class="l-col-3 is_colleague">

      <?php if (has_post_thumbnail()) : ?>
        <div class="c-colleague-hero__avatar">
          <?php the_post_thumbnail('full'); ?>
        </div>
      <?php else : ?>
        <div class="c-colleague-hero__avatar">
          <?php get_template_part('library/images/anoniem.svg'); ?>
        </div>
      <?php endif; ?>

      <div class="e-button__holder">
        <?php if (get_field('linkedin_url')) : ?>
          <a class="e-button e-button--icon e-button--blue" target="_blank" href="<?php the_field('linkedin_url'); ?>"><?php get_template_part('dist/icons/linked-in.svg'); ?></a>
        <?php endif; ?>
        <?php if (get_field('mit_colleague_email')) : ?>
          <a class="e-button e-button--icon e-button--blue" target="_blank" href="mailto:<?php the_field('mit_colleague_email'); ?>"><?php get_template_part('dist/icons/mail.svg'); ?></a>
        <?php endif; ?>
        <?php if (get_field('mit_colleage_twitter_url')) : ?>
          <a target="_blank" class="e-button e-button--icon e-button--blue" href="<?php the_field('mit_colleage_twitter_url'); ?>"><?php get_template_part('dist/icons/twitter.svg'); ?></a>
        <?php endif; ?>
      </div>

    </div>

    <div class="l-col-6">

      <?php $mit_colleage_quote_self = get_field('mit_colleage_quote_self');
      if ($mit_colleage_quote_self) :
        echo '      <blockquote>' . $mit_colleage_quote_self . '</blockquote>' . "\n" . '      ';
      endif;

      the_content(); ?>

    </div>

    <div class="l-col-3">

      <?php $metis_afdeling = get_field('metis_afdeling');
      if ($metis_afdeling == 'a') :

        $expertises = get_field('expertise');
        $expertisee = get_field('expertise_network');
        if ($expertises) :
          $args = array(
            'post_type' => 'expertises',
            'posts_per_page' => -1,
            'post__in' => $expertises
          );
          $query = new WP_Query($args);
          if ($query->have_posts()) : ?>
            <ul class="c-colleague-hero__expertise-list">
              <?php while ($query->have_posts()) : $query->the_post();
                $link_to_page = get_field('link_to_page');
                if ($link_to_page) :  ?>
                  <li class="has_link"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                <?php else : ?>
                  <li><?php the_title(); ?></li>
              <?php endif;
              endwhile; ?>
            </ul>
          <?php endif;
          wp_reset_postdata();
        endif;

        if ($expertisee) : ?>
          <ul class="c-colleague-hero__expertise-list">
            <?php foreach ($expertise as $item) : ?>
              <li><?php echo $item; ?></li>
            <?php endforeach; ?>
          </ul>
          <?php endif;
      else :
        $expertise = get_field('expertise_network');
        if ($expertise) :
          $args = array(
            'post_type' => 'expertises',
            'posts_per_page' => -1,
            'post__in' => $expertise
          );
          $query = new WP_Query($args);
          if ($query->have_posts()) : ?>
            <ul class="c-colleague-hero__expertise-list">
              <?php while ($query->have_posts()) : $query->the_post();
                $link_to_page = get_field('link_to_page');
                if ($link_to_page) :  ?>
                  <li class="has_link"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                <?php else : ?>
                  <li><?php the_title(); ?></li>
              <?php endif;
              endwhile; ?>
            </ul>
      <?php endif;
          wp_reset_postdata();
        endif;

      endif; ?>

    </div>

  </div>

</section>