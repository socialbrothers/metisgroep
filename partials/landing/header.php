<?php $header_option = get_field('mit_landing_header_optie'); ?>
<?php
  if ( $header_option == 'Optie 1' ) {
    $header_class = 'header-1';
  } else if ( $header_option == 'Optie 2' ) {
    $header_class = 'header-2';
  } else if ( $header_option == 'Optie 3' ) {
    $header_class = 'header-3';
  } else if ( $header_option == 'Optie 4' ) {
    $header_class = 'header-4';
  } else if ( $header_option == 'Optie 5' ) {
    $header_class = 'header-5';
  } else if ( $header_option == 'Optie 6' ) {
    $header_class = 'header-6';
  }
?>
<section class="c-landing-header <?php echo $header_class; ?>">
  <div class="l-container">
    <div class="l-col">
      <div class="c-landing-header__top">
        <div class="floatleft">
          <h1 class="e-heading e-heading--1"><?php the_field('mit_landing_header_title'); ?></h1>
        </div>
        <div class="floatright">
	      <?php
	      /*
		  * Enable shortcodes for this field
		  */
	      $shortcode = get_post_meta($post->ID,'mit_landing_header_shortcode_right',true);
	      echo do_shortcode($shortcode);
	      ?>
        </div>
      </div>
      <div class="c-landing-header__bottom">
        <h2 class="e-heading e-heading--2"><?php the_field('mit_landing_header_subtitle'); ?></h2>
        <a href="<?php the_field('mit_landing_header_cta_link') ?>" title="<?php the_field('mit_landing_header_cta_text'); ?>" class="e-button"><span><?php the_field('mit_landing_header_cta_text'); ?></span></a>
      </div>
    </div>
  </div>
  <div class="c-landing-header__img"></div>
  <div class="c-landing-header__bg-color"></div>
</section>