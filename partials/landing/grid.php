
<?php if( have_rows('mit_content') ): ?>

  <section class="c-page-section c-landing__content c-post-single__content height-calc-plus">

    <?php while ( have_rows('mit_content') ) : the_row(); ?>

      <?php if( get_row_layout() == 'mit_content_grid_1' ): ?>

        <div class="l-container">
          <?php the_sub_field('mit_content_grid_1_kolom_1'); ?>
        </div>

      <?php elseif( get_row_layout() == 'mit_content_grid_2' ): ?>

        <div class="l-container">
          <div class="l-col-6">
            <?php the_sub_field('mit_content_grid_2_kolom_1'); ?>
          </div>
          <div class="l-col-6">
            <?php the_sub_field('mit_content_grid_2_kolom_2'); ?>
          </div>
        </div>

      <?php elseif( get_row_layout() == 'mit_content_grid_3' ): ?>

        <div class="l-container">
          <div class="l-col-4">
            <?php the_sub_field('mit_content_grid_3_kolom_1_new'); ?>
          </div>
          <div class="l-col-4">
            <?php the_sub_field('mit_content_grid_3_kolom_2'); ?>
          </div>
          <div class="l-col-4">
            <?php the_sub_field('mit_content_grid_3_kolom_3'); ?>
          </div>
        </div>

      <?php endif; ?>

    <?php endwhile; ?>

  </section>

<?php endif; ?>
