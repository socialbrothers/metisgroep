<section id="subfooter" class="c-page-section grey">

  <div class="l-container">
    <div class="l-col-3">
      <h3><?php echo get_field("interesse", "option")["title"]; ?></h3>
    </div>

    <div class="l-col-4 m-col-6">
      <p><?php echo get_field("interesse", "option")["text"] ?></p>
    </div>

    <div class="l-col-4 m-col">
      <a class="e-button e-button--pink" href="mailto:<?php the_field('mit_email_metis_network', 'option'); ?>"><span>E-mail nu</span></a>
      <a class="e-button e-button--blue" href="<?php echo get_the_permalink(5627); ?>">Contact</a>
    </div>
  </div>

</section>