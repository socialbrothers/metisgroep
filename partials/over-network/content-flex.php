<?php

      $content_flex = get_field('content_flex');
      if(!empty( $content_flex )) : ?>

      </section>

      <section class="c-page-section no_bgimg">
        <div class="l-container">
          <div class="l-col-6 c-page-intro__content">
            <?php echo $content_flex; ?>
          </div>
        </div>

<?php endif; // !empty( $content_flex ) ?>
