<?php

  /*
  * Loop arguments & query posts
  */
  $metis_afdeling = get_field('metis_afdeling');
  if( $metis_afdeling == 'a' ) :
    $afdeling_nu      = 'metis_it';
    $afdeling_display = 'Metis IT';
  else :
    $afdeling_nu      = 'metis_network';
    $afdeling_display = 'Metis Network';
  endif;

  $args = array(
    'posts_per_page' => 4,
    'post_type'      => 'medewerkers',
    'meta_key'       => 'metis_afdeling',
    'meta_value'     => $metis_afdeling,
    'orderby'          => 'rand'
  );

  $the_query = new WP_Query( $args );

  /*
  * The loop (Nieuws)
  */
  if ( $the_query->have_posts() ) :
?>
  <section class="c-page-section c-team pink padding-eq">
    <div class="l-container">
      <div class="l-col">
        <h1 class="e-heading e-heading--1">Metis mensen</h1>
      </div>

<?php

      while ( $the_query->have_posts() ) : $the_query->the_post();

        get_template_part('partials/shared/colleague');

      endwhile;
      wp_reset_postdata();

?>
      <div class="l-col">
        <a class="e-button" href="<?php echo get_the_permalink(7275); ?>?afdeling=<?php echo $afdeling_nu; ?>" title="Alle <?php echo $afdeling_display; ?> Mensen"><span>Alle <?php echo $afdeling_display; ?> Mensen</span></a>
      </div>

    </div>
  </section>

<?php endif; ?>
