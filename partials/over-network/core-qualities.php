<section id="rollen" class="height-calc-plus">

  <div class="l-container">
    <h1 class="e-heading e-heading--1"></h1>
    <p class="e-paragraph e-paragraph--large"></p>
  </div>

  <div class="l-container c-rolls__list">
    <div class="l-col-3">
      <div class="c-rolls__icon">

      </div>
      <h3 class="e-heading e-heading--3">Principal consultant</h3>
      <p class="e-paragraph">Onafhankelijk vertrouwenspersoon voor het CEO, IT manager of <br>
projectleider.</p>
    </div>
    <div class="l-col-3">
      <div class="c-rolls__icon">

      </div>
      <h3 class="e-heading e-heading--3">IT architect</h3>
      <p class="e-paragraph">Vertaalt business doelstellingen naar IT Roadmap en infrastructuur <br>
ontwerp.  </p>
    </div>
    <div class="l-col-3">
      <div class="c-rolls__icon">

      </div>
      <h3 class="e-heading e-heading--3">Project Lead</h3>
      <p class="e-paragraph">Vormt team van specialisten en stuurt die aan in de realisatiefase.<br>
</p>
    </div>
    <div class="l-col-3">
      <div class="c-rolls__icon">

      </div>
      <h3 class="e-heading e-heading--3">Project Engineer</h3>
      <p class="e-paragraph">Implementeert het ontwerp: van aansluiten tot configureren en testen.  </p>
    </div>
  </div>

</section>
