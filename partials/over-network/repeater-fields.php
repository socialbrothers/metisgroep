<?php if( have_rows('blokken_flex') ):
        while ( have_rows('blokken_flex') ) : the_row();

          if( get_row_layout() == 'blokken_met_icoon' ):

            get_template_part('partials/over-network/repeater/blok-icoon');

          elseif( get_row_layout() == 'blokken' ):

            get_template_part('partials/over-network/repeater/blok');

          elseif( get_row_layout() == 'testimonial' ):

            get_template_part('partials/over-network/repeater/testimonials');

          elseif( get_row_layout() == 'highlights' ):

            get_template_part('partials/over-network/repeater/highlights');

          elseif( get_row_layout() == 'blokken_met_kleurverloop' ):

            get_template_part('partials/over-network/repeater/blok-kleur');

          endif; // get_row_layout()

        endwhile; // have_rows('blokken')
      endif; // have_rows('blokken') ?>
              </section>
