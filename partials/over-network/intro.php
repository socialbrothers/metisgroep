<?php $header_optie = get_field('header_optie');

      if( $header_optie == 'a' ):
        $bg_color = '';
        $content_width = 'l-col-8';
      elseif( $header_optie == 'b' ):
        $bg_color = ' grijs c-landing__content';
        $content_width = 'l-col-8';
      elseif( $header_optie == 'c' ):
        $bg_color = '';
        $content_width = 'l-col-8';
      elseif( $header_optie == 'd' ):
        $bg_color = '';
        $content_width = 'l-col-8';
      elseif( $header_optie == 'e' ):
        $bg_color = '';
        $content_width = 'l-col-8';
      elseif( $header_optie == 'f' ):
        $bg_color = '';
        $content_width = 'l-col-8';
      else :
        $bg_color = '';
        $content_width = 'l-col-8';
      endif;  ?>

<section id="intro" class="c-page-section c-landing__network height-calc-plus<?php echo $bg_color; ?>">

  <div class="l-container">
    <div class="l-col-8">
      <h4 class="c-post-single__category">Metis Network</h4>
      <h1 class="e-heading e-heading--1"><?php the_title(); ?></h1>
    </div>
    <div class="<?php echo $content_width; ?> c-page-intro__content">
      <?php the_content(); ?>
    </div>
  </div>
