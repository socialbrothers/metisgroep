<?php $nieuwe_sectie = get_sub_field('nieuwe_sectie');
      $achtergrondkleur = get_sub_field('achtergrondkleur');
      if( $achtergrondkleur  == 'b' ):
        $bg_color = ' grijs';
      else :
        $bg_color = ' wit';
      endif;

if(!empty( $nieuwe_sectie )) : ?>
        </section>

        <section class="c-page-section padding-eq<?php echo $bg_color; ?>">
<?php endif; // !empty( $blokken_titel )

      if ( have_rows('blokken') ): ?>

        <div class="l-container">
<?php   while ( have_rows('blokken') ) : the_row();
          $icoon = get_sub_field('icoon');
          $titel = get_sub_field('titel');
          $text  = get_sub_field('text');

?>
            <div class="l-col-3">
              <div class="c-rolls__icon txt_align__center">
                <?php if( $icoon == 'a' ):
                        get_template_part('dist/icons/network/experts.svg');
                      elseif( $icoon == 'b' ) :
                        get_template_part('dist/icons/network/onafhankelijk.svg');
                      elseif( $icoon == 'c' ) :
                        get_template_part('dist/icons/highlight-implementation.svg');
                      else :
                        get_template_part('dist/icons/network/verwachtingen.svg');
                      endif; ?>
              </div>
              <h3 class="e-heading e-heading--3 txt_align__center"><?php echo $titel; ?></h3>
              <p class="e-paragraph"><?php echo $text; ?></p>
            </div>
<?php

         endwhile; // have_rows('blokken')
       endif; // have_rows('blokken')

?>
          </div>
