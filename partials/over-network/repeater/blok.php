<?php $nieuwe_sectie = get_sub_field('nieuwe_sectie');
      $achtergrondkleur = get_sub_field('achtergrondkleur');
      //echo $achtergrondkleur;
      if( $achtergrondkleur  == 'b' ):
        $bg_color = ' grijs';
      else :
        $bg_color = ' wit';
      endif;

      if(!empty( $nieuwe_sectie )) : ?>
        </section>

        <section class="c-page-section<?php echo $bg_color; ?>">
<?php endif; // !empty( $blokken_titel )

      $blokken_titel = get_sub_field('blokken_titel');
      if(!empty( $blokken_titel )) : ?>
          <div class="l-container c-page-section__title">
            <div class="l-col">
              <h1 class="e-heading e-heading--1"><?php echo $blokken_titel; ?></h1>
            </div>
          </div>
<?php endif; // !empty( $blokken_titel ) ?>

          <div class="l-container">
<?php

      if ( have_rows('blokken') ):
        while ( have_rows('blokken') ) : the_row();
          $titel = get_sub_field('titel');
          $text  = get_sub_field('text');

?>
            <div class="l-col-4 m-col-6">
              <div class="c-text-block white grijs">
                <div class="c-text-block__inner ">
                  <h5 class="e-heading e-heading--5"><?php echo $titel; ?></h5>
                  <p class="e-paragraph"><?php echo $text; ?></p>
                </div>
                <div class="c-text-block__background"></div>
              </div>
            </div>
<?php

         endwhile; // have_rows('blokken')
       endif; // have_rows('blokken')

?>
          </div>
