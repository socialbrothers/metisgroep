</section>

<section class="c-home-testimonials">

  <div class="l-container">

    <?php
    $id = get_sub_field("quote")->ID;
    /*
      * Loop arguments & query posts
      */
    $args = array(
      'showposts' => 1,
      'orderby' => 'rand',
      'post_type' => 'klanten',
    );

    if (isset($id)) {
      $args["p"] = $id;
    }

    $the_query = new WP_Query($args);

    /*
      * The loop (Nieuws)
      */
    if ($the_query->have_posts()) :
    ?>

      <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

        <?php $quote = get_field('mit_client_quote_body'); ?>
        <?php if ($quote) : ?>

          <div class="c-home-testimonials__item">
            <p class="c-quote__quote"><?php echo $quote; ?></p>
            <p class="c-quote__from"><?php the_field('mit_client_quote_from'); ?></p>
            <p class="c-quote__function"><?php the_field('mit_client_quote_function'); ?></p>
          </div>

        <?php endif; ?>

      <?php
      endwhile;
      wp_reset_postdata();
      ?>

    <?php endif; ?>

  </div>