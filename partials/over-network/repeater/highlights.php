</section>
<?php
$blokken_flex = get_field('blokken_flex')[0];
?>
<?php $page_id = 7278;
$mit_front_page_highlight_alle_cta_link = get_field("mit_front_page_highlight_alle_cta_link", $page_id); ?>
<section class="c-home-highlights">

  <div class="l-container c-flexbox">

    <div class="l-col-3 c-home-highlights__item">
      <div class="c-home-highlights__icon">
        <?php get_template_part('icons/over-ons-role-one-pink.svg'); ?>
      </div>
      <h2 class="e-heading e-heading--3"><?php echo $blokken_flex['mit_front_page_highlight_one_title']; ?></h2>
      <p class="e-paragraph is_pink"><?php echo $blokken_flex['mit_front_page_highlight_one_body']; ?></p>

      <?php $expertise_ids1 = $blokken_flex['highlight_one_expertises'];

      $args_1 = array(
        'post_type'      => 'expertises',
        'posts_per_page' => -1,
        'post__in'       => $expertise_ids1,
        'orderby'        => 'post__in',
      );
      $query_1 = new WP_Query($args_1);
      if ($query_1->have_posts()) : ?>
        <ul class="c-colleague-hero__expertise-list is_pink">
          <?php while ($query_1->have_posts()) : $query_1->the_post();
            $link_to_page = get_field('link_to_page');
            if (!empty($link_to_page)) :  ?>
              <li class="has_link"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
            <?php else : ?>
              <li><?php the_title(); ?></li>
          <?php endif;
          endwhile; ?>
        </ul>
      <?php endif;
      wp_reset_postdata(); ?>
    </div>

    <div class="l-col-3 c-home-highlights__item">
      <div class="c-home-highlights__item--mobile-content">
        <div class="c-home-highlights__icon">
          <?php get_template_part('icons/over-ons-role-two-pink.svg'); ?>
        </div>
        <h2 class="e-heading e-heading--3"><?php echo $blokken_flex['mit_front_page_highlight_two_title']; ?></h2>
        <p class="e-paragraph is_pink"><?php echo $blokken_flex['mit_front_page_highlight_two_body']; ?></p>

        <?php $expertise_ids2 = $blokken_flex['highlight_two_expertises'];
        $args_2 = array(
          'post_type'      => 'expertises',
          'posts_per_page' => -1,
          'post__in'       => $expertise_ids2,
          'orderby'        => 'post__in',
        );
        $query_2 = new WP_Query($args_2);
        if ($query_2->have_posts()) : ?>
          <ul class="c-colleague-hero__expertise-list is_pink">
            <?php while ($query_2->have_posts()) : $query_2->the_post();
              $link_to_page = get_field('link_to_page');
              if (!empty($link_to_page)) :  ?>
                <li class="has_link"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
              <?php else : ?>
                <li><?php the_title(); ?></li>
            <?php endif;
            endwhile; ?>
          </ul>
        <?php endif;
        wp_reset_postdata(); ?>
      </div>
      <div class="c-home-highlights__item--mobile-bg"></div>
    </div>

    <div class="l-col-3 c-home-highlights__item">
      <div class="c-home-highlights__icon">
        <?php get_template_part('icons/over-ons-role-three-pink.svg'); ?>
      </div>
      <h2 class="e-heading e-heading--3"><?php echo $blokken_flex['mit_front_page_highlight_three_title']; ?></h2>
      <p class="e-paragraph is_pink"><?php echo $blokken_flex['mit_front_page_highlight_three_body']; ?></p>

      <?php $expertise_ids3 = $blokken_flex['highlight_three_expertises'];
      $args_3 = array(
        'post_type'      => 'expertises',
        'posts_per_page' => -1,
        'post__in'       => $expertise_ids3,
        'orderby'        => 'post__in',
      );
      $query_3 = new WP_Query($args_3);
      if ($query_3->have_posts()) : ?>
        <ul class="c-colleague-hero__expertise-list is_pink">
          <?php while ($query_3->have_posts()) : $query_3->the_post();
            $link_to_page = get_field('link_to_page');
            if (!empty($link_to_page)) :  ?>
              <li class="has_link"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
            <?php else : ?>
              <li><?php the_title(); ?></li>
          <?php endif;
          endwhile; ?>
        </ul>
      <?php endif;
      wp_reset_postdata(); ?>
    </div>

    <div class="l-col-3 c-home-highlights__item">
      <div class="c-home-highlights__item--mobile-content">
        <div class="c-home-highlights__icon">
          <?php get_template_part('icons/over-ons-role-four-pink.svg'); ?>
        </div>
        <h2 class="e-heading e-heading--3"><?php echo $blokken_flex['mit_front_page_highlight_four_title']; ?></h2>
        <p class="e-paragraph is_pink"><?php echo $blokken_flex['mit_front_page_highlight_four_body']; ?></p>

        <?php $expertise_ids4 = $blokken_flex['highlight_four_expertises'];
        $args_4 = array(
          'post_type'      => 'expertises',
          'posts_per_page' => -1,
          'post__in'       => $expertise_ids4,
          'orderby'        => 'post__in',
        );
        $query_4 = new WP_Query($args_4);
        if ($query_4->have_posts()) : ?>
          <ul class="c-colleague-hero__expertise-list is_pink">
            <?php while ($query_4->have_posts()) : $query_4->the_post();
              $link_to_page = get_field('link_to_page');
              if (!empty($link_to_page)) :  ?>
                <li class="has_link"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
              <?php else : ?>
                <li><?php the_title(); ?></li>
            <?php endif;
            endwhile; ?>
          </ul>
        <?php endif;
        wp_reset_postdata(); ?>
      </div>
      <div class="c-home-highlights__item--mobile-bg"></div>
    </div>