

<?php $nieuwe_sectie = get_sub_field('nieuwe_sectie');
      $achtergrondkleur = get_sub_field('achtergrondkleur');
      if( $achtergrondkleur  == 'b' ):
        $bg_color = ' grijs';
      else :
        $bg_color = ' wit';
      endif;

      if(!empty( $nieuwe_sectie )) : ?>
        </section>

        <section class="c-page-section c-landing__content no_bgimg padding-eq<?php echo $bg_color; ?>">
<?php endif; // !empty( $blokken_titel ) ?>

          <div class="l-container  padding-eq">
<?php

      if ( have_rows('blokken') ):
        while ( have_rows('blokken') ) : the_row();
          $titel = get_sub_field('titel');
          $text  = get_sub_field('text');

?>
            <div class="l-col-4">
              <h1><b><?php echo $titel; ?></b></h1>
              <p><?php echo $text; ?></p>
            </div>
<?php

         endwhile; // have_rows('blokken')
       endif; // have_rows('blokken')

?>
          </div>
