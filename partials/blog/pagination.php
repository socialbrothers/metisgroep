<?php
  global $wp_query;
  $bignum = 999999999;
  if ( $wp_query->max_num_pages <= 1 )
    return;

  $page_nav = paginate_links( array(
    'base'         => str_replace( $bignum, '%#%', esc_url( get_pagenum_link($bignum) ) ),
    'format'       => '?paged=%#%',
    'current'      => max( 1, get_query_var('paged') ),
    'total'        => $wp_query->max_num_pages,
    'prev_text'    => 'Nieuwer<span>e berichten</span>',
    'next_text'    => 'Ouder<span>e berichten</span>',
    'type'         => 'array',
    'end_size'     => 2,
    'mid_size'     => 1
  ) );

?>

<section class="c-page-navigation">
  <div class="l-container">
    <div class="l-col">
      <ul id="mit_page_nav" class="c-page-navigation__list">

        <?php foreach ($page_nav as $nav_item) : ?>
          <li class="c-page-navigation__list--item">
            <?php echo $nav_item;?>
          </li>
        <?php endforeach; ?>

      </ul>
    </div>
  </div>
</section>
