<section class="c-blog-header">

  <div class="l-container">

    <div class="l-col">

      <?php if ( is_post_type_archive( 'events' ) ) : ?>

        <h1 class="e-heading e-heading--1">Evenementen</h1>

      <?php else: ?>

        <h1 class="e-heading e-heading--1">Nieuws</h1>

      <?php endif; ?>

      <?php if ( !is_post_type_archive( 'events' ) ) : ?>

        <div class="c-blog-header__filter">

          <span class="c-blog-header__filter--text">Toon:</span>

          <div id="category_check" class="e-select">

            <div class="e-select__placeholder">Alle berichten</div>

            <ul class="e-select__list">
              <li class="e-select__option"><a href="<?php echo get_page_link(74); ?>">Alle berichten</a></li>
              <?php
                  wp_list_categories( array(
                    'depth' => 1,
                    'title_li' => '',
                    'hide_empty' => 0,
                  ) );
              ?>
            </ul>

          </div>

        </div>

      <?php endif; ?>

    </div>
    
  </div>

</section>
