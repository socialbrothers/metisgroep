<section class="c-page-section grey padding-top">
  <div class="l-container">

    <?php while (have_posts()) : the_post(); ?>
      <?php get_template_part('partials/shared/post'); ?>
    <?php endwhile; ?>

  </div>
</section>
