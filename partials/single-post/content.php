<?php $background_class = ( get_field('mit_featured_show_on_single') == 1 ) ? 'post-thumb' : 'no-post-thumb' ; ?>

<section id="post_single_content" class="c-post-single__content <?php echo $background_class; ?>">

  <div class="l-container">
    <div class="l-col-6 c-post-single__content--left">
      <?php the_content(); ?>
      <?php get_template_part('partials/shared/share'); ?>
    </div>

    <div class="l-col-6 c-post-single__content--right">
      <div class="c-post-single__author">
        <?php if ( is_singular('events') ) : ?>
          <?php
            /*
            ** Configure date
            */

            $date = get_field('mit_event_date', false, false);
            $date = new DateTime($date);

            $dateformatstring = "M";
            $unixtimestamp = strtotime(get_field('mit_event_date', false, false));

          ?>
          <div class="c-home-event__date" style="border: 1px solid #30089b; margin-left: 0; margin-right: 20px;">
            <div class="c-home-event__date--month"><?php echo date_i18n($dateformatstring, $unixtimestamp); ?></div>
            <div class="c-home-event__date--day" style="color: #30089b;"><?php echo $date->format('j'); ?></div>
          </div>
          <a href="mailto:info@metisit.com?SUBJECT=Aanmelding%20voor%20<?php the_title(); ?>&BODY=Hierbij%20meld%20ik%20me%20aan%20voor%3A%20<?php the_title();?>%0D%0A%0D%0ANaam%3A%20%0D%0AE-mailadres%3A%20%0D%0ATelefoonnummer%3A%20%0D%0A%0D%0AMet%20vriendelijke%20groet,%20%0D%0A%0D" title="Aanmelden voor <?php the_title(); ?>" class="e-button e-button--pink" style="min-width: 180px; margin-top: 12px;">Aanmelden</a>
        <?php else : ?>
          <div><?php the_date(); ?></div>
          <div><?php echo get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name'); ?></div>
        <?php endif; ?>
      </div>
    </div>

  </div>
</section>
