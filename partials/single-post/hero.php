<?php $background_class = ( get_field('mit_featured_show_on_single') == 1 ) ? 'post-thumb' : 'no-post-thumb' ; ?>
<?php $post_type = get_post_type(); ?>

<section class="c-post-single__hero <?php echo $background_class; ?>">

  <div class="l-container">

    <div class="l-col">

      <div id="post_single_inner">

        <?php if ( is_singular('events') ) : ?>
          <div class="c-post-single__category">Evenement</div>
        <?php else : ?>
          <div class="c-post-single__category"><?php the_category(); ?></div>
        <?php endif; ?>

        <h1 class="e-heading e-heading--1"><?php the_title(); ?></h1>

        <?php if ( is_singular('events') ) : ?>
          <a href="mailto:info@metisit.com?SUBJECT=Aanmelding%20voor%20<?php the_title(); ?>&BODY=Hierbij%20meld%20ik%20me%20aan%20voor%3A%20<?php the_title();?>%0D%0A%0D%0ANaam%3A%20%0D%0AE-mailadres%3A%20%0D%0ATelefoonnummer%3A%20%0D%0A%0D%0AMet%20vriendelijke%20groet,%20%0D%0A%0D" title="Aanmelden voor <?php the_title(); ?>" class="e-button e-button--pink" style="min-width: 180px; margin-top: 32px;">Aanmelden</a>
        <?php endif; ?>
      </div>

<?php if ( get_field('mit_featured_show_on_single') == 1 ) : ?>
      <div id="post_single_img" class="c-post-single__image">
<?php   if( have_rows('mit_featured') ):
          while ( have_rows('mit_featured') ) : the_row();

            if( get_row_layout() == 'mit_featured_image' ):

              $image = get_sub_field('mit_featured_image_src');
              if( !empty($image) ): ?>
                  <img src="<?php echo $image['url']; ?>" height="<?php echo $image['height']; ?>" width="<?php echo $image['width']; ?>" alt="<?php echo $image['alt']; ?>">
<?php         endif;

            elseif( get_row_layout() == 'mit_featured_tangram' ):

              $tangram = get_sub_field('mit_featured_tangram_sort');
              get_template_part('dist/icons/tangram/'.$tangram.'.svg');

            endif;
          endwhile;
        endif; ?>
        </div>
<?php endif; ?>

    </div>

  </div>

</section>
