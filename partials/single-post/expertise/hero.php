<?php
$terms = get_the_terms($post->ID, 'expertise-type');
?>
<section id="intro" class="c-page-section c-landing__network height-calc-plus grijs c-landing__content">
  <div class="l-container">
    <div class="l-col">
      <?php if ($terms[0]->slug == 'metis-it') : ?>
        <h4 class="c-post-single__category">Metis IT</h4>
      <?php endif; ?>

      <?php if ($terms[0]->slug == 'metis-network') : ?>
        <h4 class="c-post-single__category">Metis Network</h4>

      <?php endif; ?>
      <h4 class="c-post-single__category">Expertise</h4>
      <h1 class="e-heading e-heading--1"><?php the_title(); ?></h1>
    </div>
  </div>
</section>