<section id="post_single_content" class="c-post-single__content no-post-thumb">

  <div class="l-container">
    <div class="l-col-6 c-post-single__content--left">
      <?php the_content(); ?>
    </div>

<?php $this_id = get_the_ID(); // ID current page
      $page_id = 7278; // ID expertise is linked to role
      $expertise_ids1 = get_field('highlight_one_expertises', $page_id);
      $expertise_ids2 = get_field('highlight_two_expertises', $page_id);
      $expertise_ids3 = get_field('highlight_three_expertises', $page_id);
      $expertise_ids4 = get_field('highlight_four_expertises', $page_id);

      if ( in_array($this_id, $expertise_ids1) ) :
        $expertise_ids = $expertise_ids1;
        $number = 'one';
      elseif ( in_array($this_id, $expertise_ids2) ) :
        $expertise_ids = $expertise_ids2;
        $number = 'two';
      elseif ( in_array($this_id, $expertise_ids3) ) :
        $expertise_ids = $expertise_ids3;
        $number = 'three';
      elseif ( in_array($this_id, $expertise_ids4) ) :
        $expertise_ids = $expertise_ids4;
        $number = 'four';
      else :
        $expertise_ids = '';
        $number = '0';
      endif;

      if( !empty($expertise_ids) ) : ?>

    <div class="l-col-3 c-home-highlights__item">
      <div class="c-home-highlights__icon">
        <?php get_template_part('dist/icons/over-ons-role-'. $number .'-pink.svg'); ?>
      </div>
      <h2 class="e-heading e-heading--3"><?php the_field('mit_front_page_highlight_'. $number .'_title', $page_id); ?></h2>
      <?php /* <p class="e-paragraph is_pink"><?php the_field('mit_front_page_highlight_'. $number .'_body', $page_id); ?></p> */ ?>

  <?php $args = array(
          'post_type' => 'expertises',
          'posts_per_page' => -1,
          'post__in' => $expertise_ids
        );
        $query = new WP_Query( $args );
        if ( $query->have_posts() ) : ?>
      <ul class="c-colleague-hero__expertise-list is_pink">
<?php     while ( $query->have_posts() ) : $query->the_post();
            $link_to_page = get_field('link_to_page');
            if( !empty($link_to_page) ):  ?>
        <li class="has_link"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php       else : ?>
        <li><?php the_title(); ?></li>
<?php       endif;
          endwhile; ?>
      </ul>
<?php   endif;
      wp_reset_postdata(); ?>
    </div>

<?php endif; ?>
  </div>
</section>
