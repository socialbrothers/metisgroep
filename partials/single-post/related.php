<?php

  $categories = get_the_category();

  if ( $categories ) {

    $cat_list = array();

    foreach ( $categories as $category ) {
      array_push($cat_list, $category->cat_ID);
    }

    /*
    * Loop arguments & query posts
    */
    $args = array(
      'post__not_in' => array( get_the_ID() ),
      'showposts'    => 3,
      'post_type'    => 'post',
      'cat'          => $cat_list,
    );

    $the_query = new WP_Query( $args );

  }

  /*
  * The loop (Nieuws)
  */
  if ( $the_query != null ) :
  if ( $the_query->have_posts() ) :
?>

  <section class="c-page-section grey padding-top">

    <div class="c-related__header l-container">

      <div class="l-col">

        <h1 class="e-heading e-heading--1">Gerelateerde berichten</h1>

      </div>

    </div>

    <div class="l-container c-related">

      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <?php get_template_part('partials/shared/post'); ?>

      <?php
        endwhile;
        wp_reset_postdata();
      ?>

    </div>

    <div class="c-related__footer l-container">

      <div class="l-col">

        <a class="e-button e-button--pink" href="#"><span>Al het nieuws</span></a>

      </div>

    </div>

  </section>

<?php endif; ?>
<?php endif; ?>
