<div class="l-col-4">

  <div class="c-post">

    <div class="c-post__thumb">
      <a href="<?php the_permalink(); ?>">
        <?php if( have_rows('mit_featured') ): ?>
          <?php while ( have_rows('mit_featured') ) : the_row(); ?>
            <?php if( get_row_layout() == 'mit_featured_image' ): ?>
              <?php $image = get_sub_field('mit_featured_image_src'); ?>
              <?php if( !empty($image) ): ?>
                <?php
                  $url = $image['url'];
                  $title = $image['title'];
                  $alt = $image['alt'];
                  $caption = $image['caption'];
                  $size = 'bones-thumb-300';
                  //$size = 'logo-2x';
                  $thumb = $image['sizes'][$size];
                ?>
                <img src="<?php echo $thumb; ?>" alt="">
              <?php endif; ?>
            <?php elseif( get_row_layout() == 'mit_featured_tangram' ): ?>
              <?php $tangram = get_sub_field('mit_featured_tangram_sort'); ?>
              <div class="c-post__thumb--svg-holder">
                <?php get_template_part('dist/icons/tangram/'.$tangram.'.svg'); ?>
              </div>
            <?php endif; ?>
          <?php endwhile; ?>
        <?php else: ?>
          <div class="c-post__thumb--svg-holder">
            <?php get_template_part('dist/icons/tangram/Haas.svg'); ?>
          </div>
        <?php endif; ?>
      </a>
    </div>

    <h3 class="e-heading e-heading--3"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_excerpt(); ?></a>
    <a class="e-button" href="<?php the_permalink(); ?>" title="Lees meer over <?php the_title(); ?>"><span>Lees meer</span></a>

  </div>
  
</div>
