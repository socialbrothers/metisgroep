<section id="rollen" class="c-rolls landing height-calc-plus">

  <div class="l-container">
    <h1 class="e-heading e-heading--1"><?php the_field('mit_landingspage_rolls_title'); ?></h1>
    <p class="e-paragraph e-paragraph--large"><?php the_field('mit_landingspage_rolls_body'); ?></p>
  </div>

  <div class="l-container c-rolls__list">
    <div class="l-col-3">
      <div class="c-rolls__icon">
        <?php get_template_part('dist/icons/over-ons-role-one.svg'); ?>
      </div>
      <h3 class="e-heading e-heading--3"><?php the_field('mit_about_us_rolls_one_title', 5185); ?></h3>
      <p class="e-paragraph"><?php the_field('mit_about_us_rolls_one_body', 5185); ?></p>
    </div>
    <div class="l-col-3">
      <div class="c-rolls__icon">
        <?php get_template_part('dist/icons/over-ons-role-two.svg'); ?>
      </div>
      <h3 class="e-heading e-heading--3"><?php the_field('mit_about_us_rolls_two_title', 5185); ?></h3>
      <p class="e-paragraph"><?php the_field('mit_about_us_rolls_two_body', 5185); ?></p>
    </div>
    <div class="l-col-3">
      <div class="c-rolls__icon">
        <?php get_template_part('dist/icons/over-ons-role-three.svg'); ?>
      </div>
      <h3 class="e-heading e-heading--3"><?php the_field('mit_about_us_rolls_three_title', 5185); ?></h3>
      <p class="e-paragraph"><?php the_field('mit_about_us_rolls_three_body', 5185); ?></p>
    </div>
    <div class="l-col-3">
      <div class="c-rolls__icon">
        <?php get_template_part('dist/icons/over-ons-role-four.svg'); ?>
      </div>
      <h3 class="e-heading e-heading--3"><?php the_field('mit_about_us_rolls_four_title', 5185); ?></h3>
      <p class="e-paragraph"><?php the_field('mit_about_us_rolls_four_body', 5185); ?></p>
    </div>
  </div>

</section>
