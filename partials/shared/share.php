<?php $name = (is_single()) ? 'bericht' : 'pagina' ; ?>

<?php
  global $wp;
  $current_url = home_url(add_query_arg(array(),$wp->request));
?>

<div class="c-share">
  <h4 class="e-heading">Deel dit <?php echo $name; ?></h4>
  <div class="e-button__holder">
    <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $current_url; ?>" class="e-button e-button--icon e-button--blue"><?php get_template_part('dist/icons/linked-in.svg'); ?></a>
    <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo $current_url; ?>" class="e-button e-button--icon e-button--blue"><?php get_template_part('dist/icons/facebook.svg'); ?></a>
    <a target="_blank" href="https://twitter.com/share" data-size="large" data-url="https://dev.twitter.com/web/tweet-button" data-via="twitterdev" data-related="twitterapi,twitter" class="e-button e-button--icon e-button--blue"><?php get_template_part('dist/icons/twitter.svg'); ?></a>
    <a href="mailto:?subject=<?php echo $current_url; ?> website&body=<?php bloginfo('url'); ?>" class="e-button e-button--icon e-button--blue"><?php get_template_part('dist/icons/mail.svg'); ?></a>
    <a id="whatsapp-link" href="whatsapp://send?text=<?php echo $current_url; ?>" class="e-button e-button--icon e-button--blue"><?php get_template_part('dist/icons/whatsapp.svg'); ?></a>
  </div>
</div>
