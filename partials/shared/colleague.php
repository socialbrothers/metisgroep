<?php $metis_afdeling = get_field('metis_afdeling');
      if( $metis_afdeling == 'a' ) :
        $afdeling_nu = 'METIS IT';
      else :
        $afdeling_nu = 'METIS Network';
      endif; ?>

<div class="l-col-3 <?php if(!is_page(7275)) : ?>m-col-6 <?php endif; ?>c-colleague">
  <a class="e-paragraph" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">

<?php if ( has_post_thumbnail() ) : ?>
    <div class="c-colleague__avatar">
        <?php the_post_thumbnail('medium'); ?>
    </div>
<?php else : ?>
    <div class="c-colleague__avatar">
      <?php get_template_part('library/images/anoniem.svg'); ?>
    </div>
<?php endif; ?>

    <div class="e-paragraph">
      <span class="c-colleague__name"><?php the_title(); ?></span>
      <span class="c-colleague__function"><?php the_field('functie'); if(!empty( get_field('functie') )) : echo ', '; endif; echo $afdeling_nu; ?></span>
    </div>

  </a>

</div>
