<?php if ( have_rows('blokken') ): ?>

  <section class="c-page-section padding-eq wit">
    <div class="l-container">
<?php   while ( have_rows('blokken') ) : the_row();
    $icoon = get_sub_field('icoon');
    $titel = get_sub_field('titel');
    $text  = get_sub_field('text');

?>
      <div class="l-col-3">
        <div class="c-rolls__icon txt_align__center">
          <?php if( $icoon == 'a' ):
                  get_template_part('dist/icons/network/experts.svg');
                elseif( $icoon == 'b' ) :
                  get_template_part('dist/icons/network/onafhankelijk.svg');
                elseif( $icoon == 'c' ) :
                  get_template_part('dist/icons/highlight-implementation.svg');
                else :
                  get_template_part('dist/icons/network/verwachtingen.svg');
                endif; ?>
        </div>
        <h3 class="e-heading e-heading--3 txt_align__center"><?php echo $titel; ?></h3>
        <p class="e-paragraph"><?php echo $text; ?></p>
      </div>
<?php

        endwhile; // have_rows('blokken') ?>
    </div>
  </section>
<?php endif; // have_rows('blokken') ?>
