<?php

  $today = current_time('d/M/Y');

  /*
  ** Loop arguments & query posts
  */
  $args = array(
    'showposts' => 5,
    'post_type' => 'events',
    'meta_query' => array(
      array(
        'key' => 'mit_event_date',
				'value' => date('Ymd'),
				'type' => 'DATE',
				'compare' => '>='
      ),
    ),
	  'orderby'		=> 'meta_value',
	  'order'			=> 'ASC',
  );

  $the_query = new WP_Query( $args );

  /*
  ** The loop (Events)
  */
  if ( $the_query->have_posts() ) :
?>

  <div class="c-home-events">

    <div class="l-col">

      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <div class="c-home-event">
          <?php
            /*
            ** Configure date
            */

            $date = get_field('mit_event_date', false, false);
            $date = new DateTime($date);

            $dateformatstring = "M";
            $unixtimestamp = strtotime(get_field('mit_event_date', false, false));

          ?>

          <div class="c-home-event__date">
            <div class="c-home-event__date--month"><a href="<?php the_permalink(); ?>"><?php echo date_i18n($dateformatstring, $unixtimestamp); ?></a></div>
            <div class="c-home-event__date--day"><a href="<?php the_permalink(); ?>"><?php echo $date->format('j'); ?></a></div>
          </div>

          <div class="c-home-event__title">
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          </div>

        </div>

      <?php
        endwhile;
        wp_reset_postdata();
      ?>

      <a class="e-button e-button--pink" href="https://metisit.com/evenement/"><span>Alle evenementen</span></a>

    </div>
    
  </div>

<?php endif; ?>
