<section class="c-home-techniek">

  <div class="l-container show-on-tablet">
    <h2 class="e-heading e-heading--2"><?php the_field('mit_front_page_techniek_title'); ?></h2>
  </div>

  <div class="l-container">

    <div class="c-home-techniek__left">
      <?php get_template_part('dist/icons/home-techniek.svg'); ?>
    </div>

    <div class="c-home-techniek__right">
      <h2 class="e-heading e-heading--2"><?php the_field('mit_front_page_techniek_title'); ?></h2>
      <p class="e-paragraph e-paragraph--large"><?php the_field('mit_front_page_techniek_body'); ?></p>
      <a class="e-button e-button--pink" href="<?php the_field('mit_front_page_techniek_cta_link'); ?>" title="<?php the_field('mit_front_page_techniek_cta_text'); ?>"><span><?php the_field('mit_front_page_techniek_cta_text'); ?></span></a>
    </div>

  </div>

</section>
