<section class="c-landing-header header_landing_it header-7">
<div class="c-landing-header__img"></div>
<div class="c-landing-header__bg-color"></div>
  <div class="c-background__content">
    <div class="l-container">
      <div class="c-home-intro__text floatleft">
        <h1 class="e-heading e-heading--1"><?php the_field('mit_front_page_intro_title'); ?></h1>
      </div>
      <div class="c-home-intro__text floatright">
        <?php
          /*
           * Enable shortcodes for this field
           */
          $shortcode = get_post_meta($post->ID,'mit_front_page_intro_shortcode_right',true);
          echo do_shortcode($shortcode);
        ?>
      </div>
      <div class="c-landing-header__bottom">
        <p class="e-paragraph e-paragraph--large"><?php the_field('mit_front_page_intro_text'); ?></p>
      </div>
    </div>
  </div>
</section>
