<?php $mit_front_page_highlight_alle_cta_link = get_field( "mit_front_page_highlight_alle_cta_link" ); ?>
<section class="c-home-highlights">

  <div class="l-container c-flexbox">

    <div class="l-col-3 c-home-highlights__item">
      <div class="c-home-highlights__icon">
        <?php get_template_part('dist/icons/over-ons-role-one-pink.svg'); ?>
      </div>
      <h2 class="e-heading e-heading--3"><?php the_field('mit_front_page_highlight_one_title'); ?></h2>
      <p class="e-paragraph"><?php the_field('mit_front_page_highlight_one_body'); ?></p>
    </div>

    <div class="l-col-3 c-home-highlights__item">
      <div class="c-home-highlights__item--mobile-content">
        <div class="c-home-highlights__icon">
          <?php get_template_part('dist/icons/over-ons-role-two-pink.svg'); ?>
        </div>
        <h2 class="e-heading e-heading--3"><?php the_field('mit_front_page_highlight_two_title'); ?></h2>
        <p class="e-paragraph"><?php the_field('mit_front_page_highlight_two_body'); ?></p>
      </div>
      <div class="c-home-highlights__item--mobile-bg"></div>
    </div>

    <div class="l-col-3 c-home-highlights__item">
      <div class="c-home-highlights__icon">
        <?php get_template_part('dist/icons/over-ons-role-three-pink.svg'); ?>
      </div>
      <h2 class="e-heading e-heading--3"><?php the_field('mit_front_page_highlight_three_title'); ?></h2>
      <p class="e-paragraph"><?php the_field('mit_front_page_highlight_three_body'); ?></p>
    </div>

    <div class="l-col-3 c-home-highlights__item">
      <div class="c-home-highlights__icon">
        <?php get_template_part('dist/icons/over-ons-role-four-pink.svg'); ?>
      </div>
      <h2 class="e-heading e-heading--3"><?php the_field('mit_front_page_highlight_four_title'); ?></h2>
      <p class="e-paragraph"><?php the_field('mit_front_page_highlight_four_body'); ?></p>
    </div>

    <div class="l-col">
      <a class="e-button e-button--pink" href="<?php echo get_the_permalink($mit_front_page_highlight_alle_cta_link[0]); ?>" title="<?php the_field('mit_front_page_highlight_alle_cta_text'); ?>"><span><?php the_field('mit_front_page_highlight_alle_cta_text'); ?></span></a>
    </div>

  </div>

</section>
