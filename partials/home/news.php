<?php

/*
  * Loop arguments & query posts
  */
$selectedPosts = get_field("news");
$args = array(
  'showposts' => 2,
  'post_type' => 'post',
  'post__in' => $selectedPosts,
);

if (!empty($selectedPosts)) {
  if (count($selectedPosts) > 0) {
    $args["post__in"] = $selectedPosts;
  }
}

$the_query = new WP_Query($args);

/*
  * The loop (Nieuws)
  */
if ($the_query->have_posts()) :
?>

  <div class="c-home-news">

    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

      <?php get_template_part('partials/shared/post'); ?>

    <?php
    endwhile;
    wp_reset_postdata();
    ?>

    <a class="e-button e-button--pink" href="<?php echo get_page_link(74); ?>"><span>Al het nieuws</span></a>

  </div>

<?php endif; ?>