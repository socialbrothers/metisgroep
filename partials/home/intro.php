<?php $mit_front_page_mit_text_cta_link = get_field('mit_front_page_mit_text_cta_link');
      $mit_front_page_mn_text_cta_link  = get_field('mit_front_page_mn_text_cta_link'); ?>
<section class="c-home-intro c-background">
  <div class="c-background__holder">
    <img src="<?php bloginfo('template_url'); ?>/dist/img/home-hero.png">
    <div class="c-background__holder--left"></div>
    <div class="c-background__holder--right"></div>
  </div>
  <div class="c-background__content">
    <div class="l-container">
      <div class="c-home-intro__text floatleft no_padding">
        <h1 class="e-heading e-heading--1"><?php the_field('mit_front_page_intro_title'); ?></h1>
        <p class="e-paragraph e-paragraph--large"><?php the_field('mit_front_page_intro_text'); ?></p>
        <a href="<?php the_field('mit_front_page_intro_text_cta_link'); ?>" class="e-button"><?php the_field('mit_front_page_intro_text_cta_text'); ?></a>
      </div>
      <div class="c-home-intro__text floatright no_padding">
        <?php
          /*
           * Enable shortcodes for this field
           */
          $shortcode = get_post_meta($post->ID,'mit_front_page_intro_shortcode_right',true);
          echo do_shortcode($shortcode);
        ?>
      </div>
    </div>
  </div>
</section>

<section class="c-landing-header header_landing_it c-home-intro_half header-home_it">
  <div class="c-landing-header__img floatright"></div>
  <div class="c-landing-header__bg-color"></div>
  <div class="c-background__content floatright">
    <div class="l-container">
      <div class="c-home-intro__half">
        <div class="text">
          <h1 class="e-heading e-heading--1"><?php the_field('mit_front_page_mit_title'); ?></h1>
          <p class="e-paragraph e-paragraph--large"><?php the_field('mit_front_page_mit_tekst'); ?></p>
        </div>
        <a href="<?php echo get_the_permalink( $mit_front_page_mit_text_cta_link[0] ); ?>" class="e-button"><?php the_field('mit_front_page_mit_text_cta_text'); ?></a>
      </div>
    </div>
  </div>
</section>

<section class="c-landing-header header_landing_it c-home-intro_half header-home_network">
  <div class="c-landing-header__img"></div>
  <div class="c-landing-header__bg-color"></div>
  <div class="c-background__content">
    <div class="l-container">
      <div class="c-home-intro__half">
        <div class="text">
          <h1 class="e-heading e-heading--1"><?php the_field('mit_front_page_mn_title'); ?></h1>
          <p class="e-paragraph e-paragraph--large"><?php the_field('mit_front_page_mn_tekst'); ?></p>
        </div>
        <a href="<?php echo get_the_permalink( $mit_front_page_mn_text_cta_link[0] ); ?>" class="e-button"><?php the_field('mit_front_page_mn_text_cta_text'); ?></a>
      </div>
    </div>
  </div>
</section>

<div class="clearfix"></div>
