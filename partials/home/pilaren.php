<?php $pilaar_een_cta  = get_field( 'pilaar_een_cta' );
      $pilaar_twee_cta = get_field( 'pilaar_twee_cta' );
      $pilaar_drie_cta = get_field( 'pilaar_drie_cta' );?>

<section class="c-page-section">

  <div class="l-container">

    <div class="l-col-4 txt_align__center is_pilaar">
      <h2 class="e-heading e-heading--2"><?php the_field('pilaar_een_titel'); ?></h2>
      <p class="e-paragraph more_space"><?php the_field('pilaar_een_body'); ?></p>
      <a class="e-button e-button--pink" href="<?php echo get_the_permalink( $pilaar_een_cta[0] ); ?>" title="LEES MEER"><span>LEES MEER</span></a>
    </div>

    <div class="l-col-4 txt_align__center is_pilaar">
      <h2 class="e-heading e-heading--2"><?php the_field('pilaar_twee_titel'); ?></h2>
      <p class="e-paragraph more_space"><?php the_field('pilaar_twee_body'); ?></p>
      <a class="e-button e-button--pink" href="<?php echo get_the_permalink( $pilaar_twee_cta[0] ); ?>" title="LEES MEER"><span>LEES MEER</span></a>
    </div>

    <div class="l-col-4 txt_align__center is_pilaar">
      <h2 class="e-heading e-heading--2"><?php the_field('pilaar_drie_titel'); ?></h2>
      <p class="e-paragraph more_space"><?php the_field('pilaar_drie_body'); ?></p>
      <a class="e-button e-button--pink" href="<?php echo get_the_permalink( $pilaar_drie_cta[0] ); ?>" title="LEES MEER"><span>LEES MEER</span></a>
    </div>

  </div>
</section>
