<section class="c-home-highlights">

  <div class="l-container c-flexbox">

    <div class="l-col-3 offset-1 c-home-highlights__item">

      <div class="c-home-highlights__icon">
        <?php get_template_part('dist/icons/highlight-strategy.svg'); ?>
      </div>
      <h2 class="e-heading e-heading--2"><?php the_field('mit_front_page_highlight_one_title'); ?></h2>
      <p class="e-paragraph"><?php the_field('mit_front_page_highlight_one_body'); ?></p>
      <a class="e-button e-button--pink" href="<?php the_field('mit_front_page_highlight_one_cta_link'); ?>" title="<?php the_field('mit_front_page_highlight_one_cta_text'); ?>"><span><?php the_field('mit_front_page_highlight_one_cta_text'); ?></span></a>

    </div>

    <div class="l-col-3 offset-1 c-home-highlights__item">
      <div class="c-home-highlights__item--mobile-bg"></div>

      <div class="c-home-highlights__icon">
        <?php get_template_part('dist/icons/highlight-architecture.svg'); ?>
      </div>
      <h2 class="e-heading e-heading--2"><?php the_field('mit_front_page_highlight_two_title'); ?></h2>
      <p class="e-paragraph"><?php the_field('mit_front_page_highlight_two_body'); ?></p>
      <a class="e-button e-button--pink" href="<?php the_field('mit_front_page_highlight_two_cta_link'); ?>" title="<?php the_field('mit_front_page_highlight_two_cta_text'); ?>"><span><?php the_field('mit_front_page_highlight_two_cta_text'); ?></span></a>
    </div>



    <div class="l-col-3 offset-1 c-home-highlights__item">

      <div class="c-home-highlights__icon">
        <?php get_template_part('dist/icons/highlight-implementation.svg'); ?>
      </div>

      <h2 class="e-heading e-heading--2"><?php the_field('mit_front_page_highlight_three_title'); ?></h2>
      <p class="e-paragraph"><?php the_field('mit_front_page_highlight_three_body'); ?></p>
      <a class="e-button e-button--pink" href="<?php the_field('mit_front_page_highlight_three_cta_link'); ?>" title="<?php the_field('mit_front_page_highlight_three_cta_text'); ?>"><span><?php the_field('mit_front_page_highlight_three_cta_text'); ?></span></a>

    </div>

    <div class="l-col-3 offset-1 c-home-highlights__item">
      <div class="c-home-highlights__item--mobile-bg"></div>

      <div class="c-home-highlights__icon">
        <img src="/wp-content/themes/metis-it/icons/icoon-metis-strategy.png" alt="Stratagie icon" class="strategy-fourth-icon">
      </div>
      <h2 class="e-heading e-heading--2"><?php the_field('mit_front_page_highlight_four_title'); ?></h2>
      <p class="e-paragraph"><?php the_field('mit_front_page_highlight_four_body'); ?></p>
      <a class="e-button e-button--pink" href="<?php the_field('mit_front_page_highlight_four_cta_link'); ?>" title="<?php the_field('mit_front_page_highlight_four_cta_text'); ?>"><span><?php the_field('mit_front_page_highlight_four_cta_text'); ?></span></a>

    </div>

  </div>

</section>