<section class="c-page-section grey padding-eq">

  <div class="l-container c-flexbox">

    <?php get_template_part('partials/home/news'); ?>
    <?php get_template_part('partials/home/events'); ?>

  </div>

</section>