<section class="c-team-visual">

  <div class="c-team-visual__background">
    <div class="c-team-visual__image"></div>
    <?php get_template_part('dist/icons/home-team.svg'); ?>
    <div class="c-team-visual__background--top"></div>
    <div class="c-team-visual__background--bottom"></div>
  </div>

  <div class="c-team-visual__content">
    <div class="l-container">
      <div class="c-team-visual__left">
        <h1 class="e-heading e-heading--1"><?php the_field('mit_front_page_team_title'); ?></h1>
        <p class="e-paragraph e-paragraph--large"><?php the_field('mit_front_page_team_body'); ?></p>
        <div class="e-button__holder">
          <a class="e-button" href="<?php the_field('mit_front_page_team_cta_link'); ?>" title="<?php the_field('mit_front_page_team_cta_text'); ?>"><span><?php the_field('mit_front_page_team_cta_text'); ?></span></a>
          <a class="e-button" href="<?php the_field('mit_front_page_team_cta_link_two'); ?>" title="<?php the_field('mit_front_page_team_cta_text_two'); ?>"><span><?php the_field('mit_front_page_team_cta_text_two'); ?></span></a>
        </div>
      </div>
    </div>
  </div>

</section>
