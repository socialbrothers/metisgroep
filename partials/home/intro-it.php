


<?php $header_option = get_field('mit_landing_header_optie');
      if ( $header_option == 'Optie 1' ) {
        $header_class = 'header-1';
      } else if ( $header_option == 'Optie 2' ) {
        $header_class = 'header-2';
      } else if ( $header_option == 'Optie 3' ) {
        $header_class = 'header-3';
      } else if ( $header_option == 'Optie 4' ) {
        $header_class = 'header-4';
      } else if ( $header_option == 'Optie 5' ) {
        $header_class = 'header-5';
      } else if ( $header_option == 'Optie 6' ) {
        $header_class = 'header-6';
      }
?>
<section class="c-landing-header header_landing_it <?php echo $header_class; ?>">
<div class="c-landing-header__img"></div>
<div class="c-landing-header__bg-color"></div>
  <div class="c-background__content">
    <div class="l-container">
      <div class="c-home-intro__text floatleft">
        <h1 class="e-heading e-heading--1"><?php the_field('mit_front_page_intro_title'); ?></h1>
      </div>
      <div class="c-home-intro__text floatright">
        <?php
          /*
           * Enable shortcodes for this field
           */
          $shortcode = get_post_meta($post->ID,'mit_front_page_intro_shortcode_right',true);
          echo do_shortcode($shortcode);
        ?>
      </div>
      <div class="c-landing-header__bottom">
        <p class="e-paragraph e-paragraph--large"><?php the_field('mit_front_page_intro_text'); ?></p>
      </div>
    </div>
  </div>
</section>
