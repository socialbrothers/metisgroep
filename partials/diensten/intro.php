<section id="intro" class="c-page-intro height-calc-plus diensten">

  <div class="l-container">
    <div class="l-col-6 c-page-intro__content">
      <h1 class="e-heading e-heading--1"><?php the_title(); ?></h1>
      <?php the_content(); ?>
    </div>

    <?php
      /*
      $intro_image = get_field('mit_diensten_intro_visual');
      if ( !empty($intro_image) ) :
    ?>
      <div class="c-page-intro__image">
        <img src="<?php echo $intro_image['url']; ?>" />
      </div>
    <?php endif; */?>

    <?php get_template_part('partials/diensten/roadmap'); ?>

  </div>

</section>
