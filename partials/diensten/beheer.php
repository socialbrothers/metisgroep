<section id="dienst_beheer" class="c-page-section beheer architectuur height-calc-plus">

  <div class="l-container c-page-section__title">
    <div class="l-col">
      <h1 class="e-heading e-heading--1"><?php the_field('mit_diensten_beheer_title'); ?></h1>
    </div>
  </div>

  <div class="l-container">

    <?php if (have_rows('mit_diensten_beheer_block')) : ?>
      <?php while (have_rows('mit_diensten_beheer_block')) : the_row(); ?>
        <?php if (get_row_layout() == 'mit_diensten_beheer_block_single') : ?>
          <?php $class_style = (get_sub_field('mit_diensten_beheer_block_single_style') == 'Transparant') ? 'transparent' : 'donkerpaars'; ?>
          <?php
          if (get_sub_field('mit_diensten_beheer_block_single_style') == 'Visueel') {
            $class_style = 'visual';
            $background_class = 'c-architecture-visual';
          }
          ?>

          <div id="<?php the_sub_field('mit_diensten_beheer_block_single_anchor'); ?>" class="l-col-4">
            <div class="c-text-block <?php echo $class_style; ?>">
              <div class="c-text-block__inner">
                <h5 class="e-heading e-heading--5"><?php the_sub_field('mit_diensten_beheer_block_single_title'); ?></h5>
                <p class="e-paragraph"><?php the_sub_field('mit_diensten_beheer_block_single_body'); ?></p>
              </div>
              <div class="c-text-block__background <?php echo $background_class; ?>">
                <?php if (get_sub_field('mit_diensten_beheer_block_single_style') == 'Visueel') : ?>
                  <div class="c-diensten-architectuur__image"></div>
                  <div class="c-diensten-architectuur__bg"></div>
                <?php endif; ?>
              </div>
            </div>
          </div>

        <?php endif; ?>
      <?php endwhile; ?>
    <?php endif; ?>

  </div>

</section>