<?php
  $post_type = get_post_type();
  $is_type = false;
  $is_event = false;

  if ( $post_type == 'klanten' ) {
    $is_type = true;
    $custom_link = '/klanten';
  } else if ( $post_type == 'partners' ) {
    $is_type = true;
    $custom_link = '/partners';
  } else if ( $post_type == 'medewerkers' ) {
    $is_type = true;
    $custom_link = '/over-ons/#team';
  } else if ( $post_type == 'events' ) {
    $is_event = true;
  }

?>

<div class="l-col-4">

  <div class="c-post <?php /* echo ( $is_event == true && $is_type == false) ? 'event-post' : ''; */ ?>">

    <div class="c-post__thumb">
      <?php if ( $is_event == false ) : ?>
        <a href="<?php echo ( $is_type == true && $is_event == false ) ? $custom_link : get_permalink(); ?>">
          <?php if( have_rows('mit_featured') ): ?>
            <?php while ( have_rows('mit_featured') ) : the_row(); ?>
              <?php if( get_row_layout() == 'mit_featured_image' ): ?>
                <?php $image = get_sub_field('mit_featured_image_src'); ?>
                <?php if( !empty($image) ): ?>
                  <?php
                    $url = $image['url'];
                    $title = $image['title'];
                    $alt = $image['alt'];
                    $caption = $image['caption'];
                    $size = 'bones-thumb-300';
                    $thumb = $image['sizes'][$size];
                  ?>
                  <img src="<?php echo $thumb; ?>" alt="">
                <?php endif; ?>
              <?php elseif( get_row_layout() == 'mit_featured_tangram' ): ?>
                <?php $tangram = get_sub_field('mit_featured_tangram_sort'); ?>
                <div class="c-post__thumb--svg-holder">
                  <?php get_template_part('dist/icons/tangram/'.$tangram.'.svg'); ?>
                </div>
              <?php endif; ?>
            <?php endwhile; ?>
          <?php else: ?>
            <div class="c-post__thumb--svg-holder">
              <?php get_template_part('dist/icons/tangram/Haas.svg'); ?>
            </div>
          <?php endif; ?>
        </a>
      <?php else: ?>
        <?php
          /*
          ** Configure date
          */

          $date = get_field('mit_event_date', false, false);
          $date = new DateTime($date);

          $dateformatstring = "M";
          $unixtimestamp = strtotime(get_field('mit_event_date', false, false));

        ?>
        <a href="<?php the_permalink(); ?>">
          <div class="c-home-event__date">
            <div class="c-home-event__date--month"><?php echo date_i18n($dateformatstring, $unixtimestamp); ?></div>
            <div class="c-home-event__date--day"><?php echo $date->format('j'); ?></div>
          </div>
        </a>
      <?php endif; ?>
    </div>

    <h3 class="e-heading e-heading--3">
      <a href="<?php echo ( $is_type == true && $is_event == false ) ? $custom_link : get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
    </h3>
    <a href="<?php echo ( $is_type == true && $is_event == false ) ? $custom_link : get_permalink(); ?>" title="<?php the_title(); ?>"><?php the_excerpt(); ?></a>
    <a class="e-button" href="<?php echo ( $is_type == true && $is_event == false ) ? $custom_link : get_permalink(); ?>" title="Lees meer over <?php the_title(); ?>"><span>Lees meer</span></a>

  </div>

</div>
