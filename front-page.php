<?php get_header();
if (have_posts()) :
  while (have_posts()) : the_post();

    get_template_part('partials/home/intro');

    get_template_part('partials/home/network-rollen');

    get_template_part('partials/home/testimonials');

    get_template_part('partials/over-ons/team-filtered');

    get_template_part('partials/home/news-and-events');

  endwhile;
endif;

get_footer();
