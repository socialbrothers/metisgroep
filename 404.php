<?php get_header(); ?>

			<div class="c-page-section padding-eq">

				<div class="l-container">

					<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

						<article id="post-not-found" class="hentry cf">

							<header class="article-header">

								<h1 class="e-heading e-heading--1" style="margin-bottom: 31px;">Pagina niet gevonden.</h1>

							</header>

							<section class="entry-content">

								<p class="e-paragraph e-paragraph--large" style="max-width: 560px; margin-bottom: 31px;"><?php _e( 'The article you were looking for was not found, but maybe try looking again!', 'bonestheme' ); ?></p>

							</section>

							<section class="search">

									<p><?php get_search_form(); ?></p>

							</section>

						</article>

					</main>

				</div>

			</div>

<?php get_footer(); ?>
