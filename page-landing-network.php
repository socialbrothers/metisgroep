<?php
/*
 Template Name: Landingspagina Metis Network
*/

      get_header();

      if (have_posts()) :
        while (have_posts()) : the_post();

          get_template_part('partials/home/intro-network');

          get_template_part('partials/home/highlights-network');

          get_template_part('partials/home/testimonials');

          get_template_part('partials/home/pilaren');

          get_template_part('partials/over-network/team');

          get_template_part('partials/home/news-and-events');

        endwhile;
      endif;

      get_footer(); ?>
