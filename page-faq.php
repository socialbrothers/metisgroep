<?php
/*
 Template Name: FAQ
*/
get_header(); ?>
<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>
  <div id="page-template-faq">
    <section id="" class="c-page-section c-landing__network">
      <div class="l-container">
        <div class="l-col-8">
          <h1 class="e-heading e-heading--1"><?php the_title(); ?></h1>
        </div>
      </div>
    </section>
    <div class="l-container">
      <div class="main-content">
        <?php the_content(); ?>
        <?php if (have_rows('mit_faq_items')): ?>
          <?php while (have_rows('mit_faq_items')) : the_row();
            get_template_part('partials/faq/faq-item');
            // End loop.
          endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <?php endwhile; ?>
<?php endif; ?>
<?php get_footer();