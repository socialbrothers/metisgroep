<?php
      /*
       Template Name: Over network
      */

      get_header();

      if (have_posts()) :
        while (have_posts()) : the_post();

          get_template_part('partials/over-network/intro');

            get_template_part('partials/over-network/content-flex');

          get_template_part('partials/over-network/repeater-fields');

        endwhile;
      endif;

      get_template_part('partials/over-network/footer');

      get_footer(); ?>
