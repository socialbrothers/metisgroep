<?php get_header(); ?>

	<?php
		$today = current_time('d/M/Y');

	  /*
	  ** Loop arguments & query posts
	  */
	  $args = array(
	    'showposts' => -1,
	    'post_type' => 'events',
			'meta_query' => array(
				array(
					'key' => 'mit_event_date',
					'value' => date('Ymd'),
					'type' => 'DATE',
					'compare' => '>='
				),
			),
		  'orderby'		=> 'meta_value',
		  'order'			=> 'ASC',
	  );

	  $the_query = new WP_Query( $args );

	  /*
	  ** The loop (Events)
	  */
	  if ( $the_query->have_posts() ) :
	?>

		<?php get_template_part('partials/blog/header'); ?>

		<?php /* get_template_part('partials/blog/loop'); */ ?>

		<section class="c-page-section grey padding-top">

		  <div class="l-container">

		    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

		      <?php get_template_part('partials/search/post'); ?>

		    <?php endwhile;
				wp_reset_postdata(); ?>

		  </div>

		</section>

		<?php get_template_part('partials/blog/pagination'); ?>

	<?php endif; ?>

<?php get_footer(); ?>
