<?php
/*
 Template Name: Over ons
*/
?>

<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php get_template_part('partials/over-ons/intro'); ?>

		<?php get_template_part('partials/over-ons/core-qualities'); ?>

		<?php get_template_part('partials/over-ons/rolls'); ?>

		<?php //get_template_part('partials/over-ons/team-visual'); ?>

		<?php get_template_part('partials/over-ons/team-filtered'); ?>

		<?php /*get_template_part('partials/over-ons/work-at-mit');*/ ?>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>
