<?php get_header(); ?>

	<section class="c-blog-header">

		<div class="l-container">

				<h1 class="e-heading e-heading--1"><?php _e( 'Search Results for:', 'bonestheme' ); ?></span> <?php echo esc_attr(get_search_query()); ?></h1>

		</div>

	</section>

	<section class="c-page-section grey padding-top">

	  <div class="l-container">

	    <?php while (have_posts()) : the_post(); ?>

	      <?php get_template_part('partials/search/post'); ?>

	    <?php endwhile; ?>

	  </div>

	</section>
	
	<?php get_template_part('partials/blog/pagination'); ?>

<?php get_footer(); ?>
