// const path = require("path");
// const webpack = require("webpack");
// const ExtractTextPlugin = require("extract-text-webpack-plugin");
// const UglifyJSPlugin = require("uglifyjs-webpack-plugin");

// module.exports = {
// 	entry: {
// 		app: "./assets/js/app.js",
// 		post_single: "./assets/js/post-single.js",
// 		vendors: ["jquery", "jquery-easing", "chosen-npm/public/chosen.jquery.js"],
// 	},
// 	output: {
// 		path: path.resolve(__dirname, "dist"),
// 		filename: "js/[name].bundle.js",
// 	},
// 	module: {
// 		rules: [
// 			{
// 				test: /\.scss$/,
// 				use: ExtractTextPlugin.extract({
// 					fallback: "style-loader",
// 					use: ["css-loader", "postcss-loader", "sass-loader"],
// 					publicPath: "/dist",
// 				}),
// 			},
// 			// {
// 			// 	test: /\.js$/,
// 			// 	use: [
// 			// 		{
// 			// 			loader: "babel-loader",
// 			// 			options: {
// 			// 				presets: [["es2015", "stage-0"]],
// 			// 			},
// 			// 		},
// 			// 	],
// 			// },
// 		],
// 	},
// 	plugins: [
// 		new webpack.ProvidePlugin({
// 			$: "jquery",
// 			jQuery: "jquery",
// 		}),
// 		// new webpack.optimize.CommonsChunkPlugin({
// 		// 	name: "vendors",
// 		// }),
// 	],
// };

const path = require("path");
const extract = require("mini-css-extract-plugin");
const webpack = require("webpack");

// This is main configuration object that tells Webpackw what to do.
module.exports = {
	//path to entry paint
	entry: {
		app: "./assets/js/app.js",
		post_single: "./assets/js/post-single.js",
	},
	//path and filename of the final output
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "js/[name].bundle.js",
	},
	//plugins
	plugins: [
		new extract(),
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery",
		}),
	],
	externals: {
		// require("jquery") is external and available
		//  on the global var jQuery
		jquery: "jQuery",
	},

	// loaders
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				use: {
					loader: "babel-loader",
					options: {
						presets: ["@babel/preset-env"],
					},
				},
			},
			{
				test: /\.(sa|sc|c)ss$/,
				use: [
					{
						loader: extract.loader,
					},
					{
						loader: "css-loader",
					},
					{
						loader: "sass-loader",
						options: {
							implementation: require("sass"),
						},
					},
				],
			},
		],
	},

	//default mode is production
	mode: "development",
};
