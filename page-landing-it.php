<?php
/*
 Template Name: Landingspagina Metis IT
*/

      get_header();

      if (have_posts()) :
        while (have_posts()) : the_post();

          get_template_part('partials/home/intro-it');

          get_template_part('partials/home/highlights');

          get_template_part('partials/home/testimonials');

          get_template_part('partials/home/techniek');

          get_template_part('partials/repeatable-sections/loop');

        endwhile;
      endif;

      get_footer(); ?>
