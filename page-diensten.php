<?php
/*
 Template Name: Diensten
*/
?>

<p>dfas</p>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php get_template_part('partials/diensten/intro'); ?>

		<?php get_template_part('partials/diensten/strategie'); ?>

		<?php get_template_part('partials/diensten/architectuur'); ?>

		<?php get_template_part('partials/diensten/realisatie'); ?>

		<?php get_template_part('partials/diensten/beheer'); ?>

<?php endwhile;
endif; ?>

<?php get_footer(); ?>