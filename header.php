<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
  <meta charset="utf-8">
  <?php // force Internet Explorer to use the latest rendering engine available 
  ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <?php /* post_type_archive_title(); */
  $post_type = get_post_type();
  if ($post_type == 'klanten' || $post_type == 'partners' || $post_type == 'events') :
  ?>
    <title><?php bloginfo('name'); ?> - <?php post_type_archive_title() ?></title>
  <?php else : ?>
    <title><?php wp_title(''); ?></title>
  <?php endif; ?>

  <?php // mobile meta (hooray!) 
  ?>
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) 
  ?>
  <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
  <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
  <!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
  <?php // or, set /favicon.ico for IE10 win 
  ?>
  <meta name="msapplication-TileColor" content="#f01d4f">
  <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
  <meta name="theme-color" content="#121212">

  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

  <!-- Facebook Pixel Code -->
  <script>
    ! function(f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function() {
        n.callMethod ?
          n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
    }(window, document, 'script',

      'https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '1872358759705139');

    fbq('track', 'PageView');
  </script>
  <noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=1872358759705139&ev=pageview&noscript=1" />
  </noscript>
  <!-- End Facebook Pixel Code -->

  <?php // wordpress head functions
  wp_head();
  // end of wordpress head 
  ?>

</head>
<?php
$current_website = get_field('website_it_or_network', 'options') ?? false;
?>

<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

  <div id="container">

    <header class="c-header" itemscope itemtype="http://schema.org/WPHeader">

      <div class="c-header__content">

        <div class="c-header__top">

          <div class="l-container">

            <div class="l-col">

              <p class="e-logo" itemscope itemtype="http://schema.org/Organization">
                <a href="<?php echo home_url();
                          echo $current_website ? '' : '/network/'; ?>" rel="nofollow">
                  <?php

                  if ($current_website) {
                    echo get_template_part('icons/logo-it');
                  } else {
                    echo get_template_part('icons/logo-network');
                  }
                  ?>
                </a>
              </p>

              <div class="c-header__indicator">
                <span class="c-header__indicator--text">Menu</span>
                <figure class="c-header__triangle"></figure>
              </div>

              <nav itemscope itemtype="http://schema.org/SiteNavigationElement">
                <?php wp_nav_menu(array(
                  'container' => false,                           // remove nav container
                  'container_class' => 'menu cf',                 // class of container (should you choose to use it)
                  'menu' => __('The Main Menu', 'bonestheme'),  // nav name
                  'menu_class' => 'c-menu top-nav cf',               // adding custom nav class
                  'theme_location' => 'main-nav',                 // where it's located in the theme
                  'before' => '',                                 // before the menu
                  'after' => '',                                  // after the menu
                  'link_before' => '',                            // before each link
                  'link_after' => '',                             // after each link
                  'depth' => 0,                                   // limit the depth of the nav
                  'fallback_cb' => ''                             // fallback function (if there is one)
                )); ?>
              </nav>

            </div>

          </div>

        </div>

        <div class="c-header__bottom">

          <div class="l-container">

            <div class="l-col">

              <div class="c-header__bottom--left">
                <a href="<?php echo esc_url(get_page_link(5627)); ?>" class="e-button e-button--grey c-header__button-contact"><span>Contact</span></a>
                <div class="c-header__mobile-contact">
                  <a href="tel:+31850080240" class="e-button e-button--icon e-button--grey"><?php get_template_part('dist/icons/phone.svg'); ?></a>
                  <a href="<?php echo esc_url(get_page_link(5627)); ?>" class="e-button e-button--icon e-button--grey"><?php get_template_part('dist/icons/location.svg'); ?></a>
                </div>
                <p class="c-header__address">085 008 0240</p>
              </div>

              <div class="c-header__bottom--right">
                <?php get_search_form(); ?>
              </div>

            </div>

          </div>

        </div>

      </div>

    </header>

    <div class="c-header--placeholder"></div>