<?php
/*
 Template Name: Mensen
*/

      get_header();

      if (have_posts()) :
        while (have_posts()) : the_post(); ?>
<section class="c-colleague-hero bg-no-repeat">
	<div class="l-container">

		<div class="l-col-6 c-colleague-hero__title">
			<h1 class="e-heading e-heading--1"><?php the_title(); ?></h1>

<?php if(isset($_GET['afdeling']) && $_GET['afdeling'] == 'metis_it'):
        $it_checked = ' checked="checked"';
        $nw_checked = '';
      elseif(isset($_GET['afdeling']) && $_GET['afdeling'] == 'metis_network'):
        $it_checked = '';
        $nw_checked = ' checked="checked"';
      elseif(!isset($_GET['afdeling']) || $_GET['afdeling'] == ''):
        $it_checked = ' checked="checked"';
        $nw_checked = ' checked="checked"';
      endif; ?>

          <div class="colleague_filter">
            <span class="filter">
              <input type="checkbox" name="metis_it" id="metis_it" data-target="?afdeling=metis_it"<?php echo $it_checked; ?>>
              <label for="metis_it">Metis IT</label>
            </span>
            <span class="filter">
              <input type="checkbox" name="metis_network" id="metis_network" data-target="?afdeling=metis_network"<?php echo $nw_checked; ?>>
              <label for="metis_network">Metis Network</label>
            </span>
          </div>

          <script>
          jQuery(function(){
            jQuery(".filter").on('change', 'input[type="checkbox"]', function(){
              var item=jQuery(this),
                  sibling = item.parent('.filter').siblings('.filter').children('input');

              if(item.is(":checked") === true && sibling.is(":checked") === false) {
                window.location.href= item.data("target");
              }

              else if(item.is(":checked") === false && sibling.is(":checked") === true){
                window.location.href= sibling.data("target");
              }

              else if( (item.is(":checked") === false && sibling.is(":checked") === false) || (item.is(":checked") === true && sibling.is(":checked") === true) ){
                window.location.href= '<?php echo get_the_permalink(get_the_ID()); ?>';
              }
            });
          });
          </script>
		</div>

	</div>

	<?php get_template_part('partials/over-ons/team'); ?>
  </section>
<?php   endwhile;
      endif;

      get_footer(); ?>
