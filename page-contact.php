<?php
/*
 Template Name: Contact
*/
get_header();

if (have_posts()) :
  while (have_posts()) : the_post();

    $mit_email_metis = get_field('alternatief_email_adres_metis_it');
    if (empty($mit_email_metis)) :
      $mit_email_metis = get_field('mit_email_metis', 'option');
    endif;

    $mit_email_metis_network = get_field('alternatief_email_adres_metis_network');
    if (empty($mit_email_metis_network)) :
      $mit_email_metis_network = get_field('mit_email_metis_network', 'option');
    endif; ?>

    <section class="c-contact-maps">
      <h1 class="e-heading e-heading--1"><?php the_title(); ?></h1>
      <p class="e-paragraph"><?php the_field('mit_footer_contact_address', 'option'); ?></p>
      <a class="e-button" target="_blank" href="https://www.google.com/maps/dir//Metis+IT+B.V.,+Driemanssteeweg+200,+3084+CB+Rotterdam/@51.8634824,4.4609136,18.23z/data=!4m9!4m8!1m0!1m5!1m1!1s0x47c5b5a7cd584959:0xa29350e18fd16933!2m2!1d4.4619502!2d51.8636476!3e0">Route</a>
      <div id="metis_it_map"></div>
    </section>

    <section class="c-page-section padding-top grey c-contact-forms">

      <div class="l-container">

        <div class="l-col-4">
          <h4 class="e-heading e-heading--4"><?php the_field('mit_contact_col_one_title'); ?></h4>
          <?php the_field('mit_contact_col_one_body'); ?>
        </div>

        <div class="l-col-4">
          <h4 class="e-heading e-heading--4"><?php the_field('mit_contact_col_two_title'); ?></h4>
          <?php the_field('mit_contact_col_two_body'); ?>
        </div>

        <div class="l-col-4">
          <h4 class="e-heading e-heading--4"><?php the_field('mit_contact_col_three_title'); ?></h4>
          <div class="e-mail__holder">
            <a class="e-icon e--icon-button e-icon--blue" href="mailto:<?php echo $mit_email_metis; ?>"><?php get_template_part('dist/icons/mail.svg'); ?> <?php the_field('mit_emaillabel_metis_it', 'option'); ?></a>
            <a class="e-icon e--icon-button e-icon--blue" href="mailto:<?php echo $mit_email_metis_network; ?>"><?php get_template_part('dist/icons/mail.svg'); ?> <?php the_field('mit_emaillabel_metis_network', 'option'); ?></a>
          </div>
          <p class="e-paragraph e-paragraph--line-height">
            <?php the_field('mit_footer_contact_address', 'option'); ?>
          </p>
          <div class="e-button__holder">
            <?php if (get_field('mit_linked_in_url', 'option')) : ?><a class="e-button e-button--icon e-button--blue" target="_blank" href="<?php the_field('mit_linked_in_url', 'option'); ?>"><?php get_template_part('dist/icons/linked-in.svg'); ?></a><?php endif; ?>
            <?php if (get_field('mit_facebook_url', 'option')) : ?><a class="e-button e-button--icon e-button--blue" target="_blank" href="<?php the_field('mit_facebook_url', 'option'); ?>"><?php get_template_part('dist/icons/facebook.svg'); ?></a><?php endif; ?>
            <?php if (get_field('mit_twitter_url', 'option')) : ?><a class="e-button e-button--icon e-button--blue" target="_blank" href="<?php the_field('mit_twitter_url', 'option'); ?>"><?php get_template_part('dist/icons/twitter.svg'); ?></a><?php endif; ?>
          </div>
        </div>

      </div>

    </section>
<?php get_page_link();

  endwhile;
endif; ?>

<script type="text/javascript">
  function initMap() {
    var uluru = {
      lat: 51.863648,
      lng: 4.461996
    };
    var map = new google.maps.Map(document.getElementById('metis_it_map'), {
      zoom: 13,
      center: uluru
    });
    var marker = new google.maps.Marker({
      position: uluru,
      map: map
    });
  }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCJxRF3221dYkEmcUbz5b3EjzOc1utS1Q&callback=initMap"></script>

<?php get_footer(); ?>