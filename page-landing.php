<?php
/*
 Template Name: Landingspagina
*/
?>

<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php get_template_part('partials/landing/header'); ?>
		<?php get_template_part('partials/landing/grid'); ?>
		<?php get_template_part('partials/shared/rolls'); ?>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>
