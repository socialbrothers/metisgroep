<?php get_header(); ?>

  <?php if (have_posts()) : ?>

    <?php get_template_part('partials/blog/header'); ?>

    <?php get_template_part('partials/blog/loop'); ?>
    
    <?php get_template_part('partials/blog/pagination'); ?>

  <?php endif; ?>

<?php get_footer(); ?>
