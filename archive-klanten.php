<?php get_header(); ?>

	<?php if (have_posts()) : ?>

		<?php get_template_part('partials/clients/intro'); ?>

		<?php get_template_part('partials/clients/highlighted'); ?>

		<?php get_template_part('partials/clients/logo-section'); ?>

	<?php endif; ?>

<?php get_footer(); ?>
