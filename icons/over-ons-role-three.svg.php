<svg width="121px" height="120px" viewBox="0 0 121 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
    <title>Group 11</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g data-id="Metis" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g data-id="Desktop-Over-Ons" transform="translate(-1050.000000, -1603.000000)">
            <g data-id="3-Rollen" transform="translate(-240.000000, 1300.000000)">
                <g data-id="Group-2" transform="translate(1220.000000, 303.000000)">
                    <g data-id="Group-11" transform="translate(70.000000, 0.000000)">
                        <polygon data-id="Fill-1" fill="#30079F" points="60.3828 0.0003 0.3838 59.9993 60.3828 120.0003 120.3828 59.9993"></polygon>
                        <path d="M73.0428,46.438 C73.0428,53.427 67.3768,59.092 60.3888,59.092 C53.3998,59.092 47.7338,53.427 47.7338,46.438 C47.7338,39.449 53.3998,33.784 60.3888,33.784 C67.3768,33.784 73.0428,39.449 73.0428,46.438 Z" data-id="Stroke-2" stroke="#FFFFFF" stroke-width="3" stroke-linejoin="round"></path>
                        <path d="M68.0443,56.4986 C72.1843,56.4986 75.2303,58.1866 76.9873,59.9896 C80.1273,63.2096 79.9963,68.2666 79.9963,73.5596 L79.9963,82.8836" data-id="Stroke-4" stroke="#FFFFFF" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"></path>
                        <path d="M45.9418,58.7803 C40.2788,61.8463 40.7808,67.1463 40.7808,73.5593 L40.7808,82.8843" data-id="Stroke-6" stroke="#FFFFFF" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
