<svg width="42px" height="42px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
    <title>search</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g data-id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g data-id="search" stroke="#30089B">
            <g data-id="Group-4" transform="translate(6.000000, 6.000000)">
                <g data-id="Group-2" transform="translate(17.000000, 17.000000) rotate(-315.000000) translate(-17.000000, -17.000000) translate(3.000000, 8.000000)">
                    <circle data-id="Oval" stroke-width="2" cx="9" cy="9" r="9"></circle>
                    <path d="M19.301209,5.53585122 L25.8561299,12.1563518" data-id="Line" stroke-width="4" transform="translate(22.544565, 8.754852) rotate(-45.000000) translate(-22.544565, -8.754852) "></path>
                </g>
            </g>
        </g>
    </g>
</svg>
