<svg width="121px" height="120px" viewBox="0 0 121 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
    <title>Group 9</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g data-id="Metis" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g data-id="Desktop-Over-Ons" transform="translate(-1350.000000, -1603.000000)">
            <g data-id="3-Rollen" transform="translate(-240.000000, 1300.000000)">
                <g data-id="Group" transform="translate(1520.000000, 303.000000)">
                    <g data-id="Group-9" transform="translate(70.000000, 0.000000)">
                        <polygon data-id="Fill-31" fill="#F65275" points="60.3832 120 0.3832 60 60.3832 0 120.3832 60"></polygon>
                        <path d="M73.0375,44.0562 C73.0375,51.0452 67.3715,56.7102 60.3835,56.7102 C53.3945,56.7102 47.7285,51.0452 47.7285,44.0562 C47.7285,37.0672 53.3945,31.4022 60.3835,31.4022 C67.3715,31.4022 73.0375,37.0672 73.0375,44.0562 Z" data-id="Stroke-33" stroke="#FFFFFF" stroke-width="3" stroke-linejoin="round"></path>
                        <path d="M83.0873,79.2281 C83.0873,69.5281 81.4623,61.6651 69.6723,61.6651" data-id="Stroke-35" stroke="#FFFFFF" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"></path>
                        <path d="M37.6791,79.2281 C37.6791,69.5281 39.3041,61.6651 51.0941,61.6651" data-id="Stroke-37" stroke="#FFFFFF" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"></path>
                        <polyline data-id="Stroke-39" stroke="#FFFFFF" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="69.6722 61.6646 60.3832 71.3676 51.0942 61.6646"></polyline>
                        <polyline data-id="Stroke-41" stroke="#FFFFFF" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="51.4384 80.312 60.3834 71.368 69.3274 80.312"></polyline>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
