<svg width="40px" height="40px" viewBox="0 0 40 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
    <title>location</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g data-id="Metis" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g data-id="location" fill="#30089B">
            <path d="M28,16.5 C28,17.3935547 27.8974609,18.3164063 27.5166016,19.1220703 L22.1845703,30.4599609 C21.8769531,31.1044922 21.203125,31.5 20.5,31.5 C19.796875,31.5 19.1230469,31.1044922 18.8300781,30.4599609 L13.4833984,19.1220703 C13.1025391,18.3164063 13,17.3935547 13,16.5 C13,12.3544922 16.3544922,9 20.5,9 C24.6455078,9 28,12.3544922 28,16.5 Z M24.25,16.5 C24.25,14.4345703 22.5654297,12.75 20.5,12.75 C18.4345703,12.75 16.75,14.4345703 16.75,16.5 C16.75,18.5654297 18.4345703,20.25 20.5,20.25 C22.5654297,20.25 24.25,18.5654297 24.25,16.5 Z" data-id="Combined-Shape"></path>
        </g>
    </g>
</svg>
