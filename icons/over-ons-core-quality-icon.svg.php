<svg width="413px" height="494px" viewBox="0 0 413 494" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
    <title>Hoofd-Tangram-brein</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g data-id="Metis" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g data-id="Desktop-Over-Ons" transform="translate(-1067.000000, -705.000000)">
            <g data-id="2-Kernkwaliteiten" transform="translate(-240.000000, 550.000000)">
                <g data-id="Hoofd-Tangram-brein" transform="translate(1307.000000, 155.000000)">
                    <path d="M405.1275,461.1185 C392.1275,432.1185 351.5695,392.6695 341.1275,356.1185 C335.1275,335.1185 416.2795,274.2835 412.2715,171.2935 C408.1065,64.3125 335.3935,-3.6865 235.4655,0.1545 C104.4655,5.1905 60.8045,55.1185 41.7925,125.7125 C38.4305,138.1955 37.5325,153.8105 41.7195,167.5665 C43.8785,174.6715 6.4735,218.7845 0.4055,240.3975 C-2.2755,249.9505 8.8075,255.5705 19.4185,258.2105 C32.1885,261.3935 38.1195,269.0535 29.6815,288.7735 C28.0405,292.5835 26.3105,296.8705 26.3105,300.2065 C26.3105,302.7265 26.7145,305.6665 27.6425,307.8885 C29.3595,311.9845 31.2045,313.0805 33.5185,314.6575 C24.4925,320.0435 27.1485,329.1875 27.7465,331.0915 C30.6845,340.3905 38.6725,344.0245 42.2675,344.6635 C44.0945,344.9875 45.5345,345.2435 46.7645,346.6115 C55.6715,356.5105 39.3155,384.7095 46.0705,405.4505 C48.3075,412.2815 53.5975,416.1265 62.2455,417.2165 C70.3315,418.2365 86.1025,416.3195 99.8625,414.7885 C177.8045,406.1185 192.8045,455.5375 187.8045,493.8275 L405.1275,461.1185 Z" data-id="Fill-1" fill="#FFFFFF"></path>
                    <polygon data-id="Fill-4" fill="#F75275" points="310 69.5360825 258.536082 121 206 69.5360825 258.536082 17"></polygon>
                    <polygon data-id="Fill-6" fill="#A73486" points="258 113 162 113 209.505155 65"></polygon>
                    <polygon data-id="Fill-8" fill="#F75275" points="113 113 209 113 161.494845 161"></polygon>
                    <polygon data-id="Fill-10" fill="#581797" points="211 161 307 161 259.494845 209"></polygon>
                    <polygon data-id="Fill-12" fill="#F75275" points="326 161 326 93 360 127"></polygon>
                    <polygon data-id="Fill-14" fill="#A73486" points="114 161 114 113 90 137"></polygon>
                    <polygon data-id="Fill-16" fill="#7F268E" points="114 113 162 161 114 161"></polygon>
                    <polygon data-id="Fill-18" fill="#F75275" points="354 65 306 113 306 65"></polygon>
                    <polygon data-id="Fill-20" fill="#CE437D" points="162.494845 113 210 65 162.494845 65 114 113"></polygon>
                    <polygon data-id="Fill-22" fill="#CE437D" points="259 208.494845 211 161 211 209.494845 259 257"></polygon>
                    <polygon data-id="Fill-24" fill="#A73486" points="306 113 258 113 306 65"></polygon>
                    <polygon data-id="Fill-26" fill="#7F268E" points="326 93 258 161 326 161"></polygon>
                    <polygon data-id="Fill-28" fill="#CE437D" points="208.331034 113 307 113 258.668966 161 161 161"></polygon>
                </g>
            </g>
        </g>
    </g>
</svg>
