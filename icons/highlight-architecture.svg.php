<svg width="120px" height="120px" viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
    <title>highlight-architecture</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g data-id="Metis" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g data-id="Desktop-HD" transform="translate(-900.000000, -668.000000)">
            <g data-id="Highlights" transform="translate(0.000000, 668.000000)">
                <g data-id="Group-6" transform="translate(780.000000, 0.000000)">
                    <g data-id="highlight-architecture" transform="translate(120.000000, 0.000000)">
                        <polygon data-id="Fill-1" fill="#30079B" points="60.0003 0.0003 0.0003 59.9993 60.0003 120.0003 119.9993 59.9993"></polygon>
                        <polygon data-id="Stroke-2" stroke="#FFFFFF" stroke-width="2.835" stroke-linejoin="round" points="38.0177 47.6367 38.0177 73.0197 59.9997 85.7107 59.9997 60.3277"></polygon>
                        <polygon data-id="Stroke-3" stroke="#FFFFFF" stroke-width="2.835" stroke-linejoin="round" points="60.0001 60.3281 60.0001 85.7111 81.9821 73.0191 81.9821 47.6371"></polygon>
                        <polygon data-id="Stroke-4" stroke="#FFFFFF" stroke-width="2.835" stroke-linejoin="round" points="60.0001 34.9453 38.0181 47.6363 60.0001 60.3283 81.9821 47.6363"></polygon>
                        <polygon data-id="Stroke-5" stroke="#FFFFFF" stroke-width="0.85" stroke-linejoin="round" points="81.982 73.0195 81.982 47.6365 60 34.9455 60 60.3285"></polygon>
                        <polygon data-id="Stroke-6" stroke="#FFFFFF" stroke-width="0.85" stroke-linejoin="round" points="60.0001 60.3281 60.0001 34.9451 38.0181 47.6371 38.0181 73.0191"></polygon>
                        <polygon data-id="Stroke-7" stroke="#FFFFFF" stroke-width="0.85" stroke-linejoin="round" points="60.0001 85.7109 81.9821 73.0199 60.0001 60.3279 38.0181 73.0199"></polygon>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
