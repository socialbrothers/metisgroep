<svg width="120px" height="120px" viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
    <title>Group 5</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g data-id="Metis" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g data-id="Desktop-Over-Ons" transform="translate(-450.000000, -1603.000000)">
            <g data-id="3-Rollen" transform="translate(-240.000000, 1300.000000)">
                <g data-id="Group-7" transform="translate(620.000000, 303.000000)">
                    <g data-id="Group-5" transform="translate(70.000000, 0.000000)">
                        <polygon data-id="Fill-8" fill="#F65275" points="59.9999 120 -0.0001 60 59.9999 0 119.9999 60"></polygon>
                        <path d="M72.6542,44.0562 C72.6542,51.0452 66.9882,56.7102 60.0002,56.7102 C53.0112,56.7102 47.3452,51.0452 47.3452,44.0562 C47.3452,37.0672 53.0112,31.4022 60.0002,31.4022 C66.9882,31.4022 72.6542,37.0672 72.6542,44.0562 Z" data-id="Stroke-11" stroke="#FFFFFF" stroke-width="3" stroke-linejoin="round"></path>
                        <path d="M82.704,79.2281 C82.704,69.5281 81.079,61.6651 69.289,61.6651" data-id="Stroke-13" stroke="#FFFFFF" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"></path>
                        <path d="M37.2958,79.2281 C37.2958,69.5281 38.9208,61.6651 50.7108,61.6651" data-id="Stroke-15" stroke="#FFFFFF" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"></path>
                        <polyline data-id="Stroke-17" stroke="#FFFFFF" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="69.2894 61.6646 59.9994 71.3676 50.7104 61.6646"></polyline>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
