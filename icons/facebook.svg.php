<svg width="40px" height="40px" viewBox="0 0 40 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
    <title>facebook</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g data-id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g data-id="facebook" fill="#30089B">
            <path d="M25.542,13.368 L23.626,13.368 C22.125,13.368 21.844,14.088 21.844,15.125 L21.844,17.431 L25.42,17.431 L24.944,21.043 L21.844,21.043 L21.844,30.304 L18.11,30.304 L18.11,21.043 L15,21.043 L15,17.431 L18.11,17.431 L18.11,14.771 C18.11,11.683 20.002,10 22.76,10 C24.078,10 25.212,10.097 25.542,10.147 L25.542,13.368 Z" data-id="Fill-20"></path>
        </g>
    </g>
</svg>
