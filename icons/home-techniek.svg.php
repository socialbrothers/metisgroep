<svg width="466px" height="436px" viewBox="0 0 466 436" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
    <title>Group</title>
    <desc>Created with Sketch.</desc>
    <defs>
        <linearGradient x1="50.0000179%" y1="-492.00984%" x2="50.0000179%" y2="76.00152%" data-id="linearGradient-1">
            <stop stop-color="#DEDEDE" offset="0%"></stop>
            <stop stop-color="#F65275" offset="15.8450704%"></stop>
            <stop stop-color="#F65275" offset="48.943662%"></stop>
            <stop stop-color="#F65275" offset="83.0985915%"></stop>
            <stop stop-color="#DEDEDE" offset="100%"></stop>
        </linearGradient>
        <linearGradient x1="50%" y1="4.19354839%" x2="50%" y2="95.8064516%" data-id="linearGradient-2">
            <stop stop-color="#DEDEDE" offset="0%"></stop>
            <stop stop-color="#F65275" offset="15.8450704%"></stop>
            <stop stop-color="#F65275" offset="48.943662%"></stop>
            <stop stop-color="#F65275" offset="83.0985915%"></stop>
            <stop stop-color="#DEDEDE" offset="100%"></stop>
        </linearGradient>
    </defs>
    <g data-id="Metis" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g data-id="Desktop-HD" transform="translate(-477.000000, -1747.000000)">
            <g data-id="Techniek" transform="translate(480.000000, 1750.000000)">
                <g data-id="Group">
                    <polygon data-id="Fill-123" fill="url(#linearGradient-1)" points="50.8893709 429.999 170.630065 429.999 170.630065 380 50.8893709 380"></polygon>
                    <polygon data-id="Stroke-125" stroke="#FFFFFF" stroke-width="6" stroke-linecap="square" points="50.8893709 430 170.629067 430 170.629067 380 50.8893709 380"></polygon>
                    <polygon data-id="Fill-127" fill="url(#linearGradient-1)" points="170.629067 429.999 290.368764 429.999 290.368764 380 170.629067 380"></polygon>
                    <polygon data-id="Stroke-129" stroke="#FFFFFF" stroke-width="6" stroke-linecap="square" points="170.629067 430 290.368764 430 290.368764 380 170.629067 380"></polygon>
                    <polygon data-id="Fill-131" fill="url(#linearGradient-1)" points="290.368764 429.999 410.10846 429.999 410.10846 380 290.368764 380"></polygon>
                    <text data-id="CPU" font-size="18"  fill="#30079F">
                        <tspan x="94.7939262" y="411">CPU</tspan>
                    </text>
                    <text data-id="Opslag" font-size="18"  fill="#30079F">
                        <tspan x="202.559653" y="411">Opslag</tspan>
                    </text>
                    <text data-id="Netwerk" font-size="18"  fill="#30079F">
                        <tspan x="317.310195" y="411">Netwerk</tspan>
                    </text>
                    <g data-id="Group-116" transform="translate(49.891540, 120.000000)">
                        <polygon data-id="Fill-109" fill="#DEDEDE" points="0.620650759 50 150.295271 50 150.295271 0 0.620650759 0"></polygon>
                        <polygon data-id="Stroke-111" stroke="#FFFFFF" stroke-width="6" points="0.620650759 50 150.295271 50 150.295271 0 0.620650759 0"></polygon>
                        <polygon data-id="Fill-112" fill="#DEDEDE" points="150.295271 50 299.969892 50 299.969892 0 150.295271 0"></polygon>
                        <polygon data-id="Stroke-113" stroke="#FFFFFF" stroke-width="6" points="150.295271 50 299.969892 50 299.969892 0 150.295271 0"></polygon>
                        <polygon data-id="Fill-114" fill="#DEDEDE" points="299.969892 50 359.83974 50 359.83974 0 299.969892 0"></polygon>
                        <polygon data-id="Stroke-115" stroke="#FFFFFF" stroke-width="6" points="299.969892 50 359.83974 50 359.83974 0 299.969892 0"></polygon>
                    </g>
                    <text data-id="Application" font-size="18"  fill="#30079F">
                        <tspan x="235.488069" y="153">Application</tspan>
                    </text>
                    <text data-id="…" font-size="21"  fill="#30079F">
                        <tspan x="370.195228" y="148">…</tspan>
                    </text>
                    <text data-id="Application" font-size="18"  fill="#30079F">
                        <tspan x="85.813449" y="153">Application</tspan>
                    </text>
                    <text data-id="OS" font-size="18"  fill="#30079F">
                        <tspan x="69.0335271" y="309.067864">OS</tspan>
                    </text>
                    <g data-id="Group-122">
                        <polygon data-id="Fill-117" fill="#30079B" points="0.620650759 120 459.62282 120 459.62282 0 0.620650759 0"></polygon>
                        <polygon data-id="Stroke-119" stroke="#FFFFFF" stroke-width="6" points="0.620650759 120 459.62282 120 459.62282 0 0.620650759 0"></polygon>
                        <polygon data-id="Fill-120" fill="#F75275" points="50.5121909 330 110.382039 330 110.382039 280 50.5121909 280"></polygon>
                        <polygon data-id="Stroke-121" stroke="#FFFFFF" stroke-width="6" points="50.5121909 330 110.382039 330 110.382039 280 50.5121909 280"></polygon>
                    </g>
                    <polygon data-id="Stroke-133" stroke="#FFFFFF" stroke-width="6" stroke-linecap="square" points="289.991584 430 409.73128 430 409.73128 380 289.991584 380"></polygon>
                    <polygon data-id="Fill-135" fill="#F75275" points="50.5121909 380 409.73128 380 409.73128 330 50.5121909 330"></polygon>
                    <polygon data-id="Stroke-136" stroke="#FFFFFF" stroke-width="6" points="50.5121909 380 409.73128 380 409.73128 330 50.5121909 330"></polygon>
                    <text data-id="Virtualisatie" font-size="18"  fill="#30079F">
                        <tspan x="184.058373" y="361.745464">Vi</tspan>
                        <tspan x="200.114373" y="361.745464" letter-spacing="0.72">r</tspan>
                        <tspan x="208.160373" y="361.745464">tualisatie</tspan>
                    </text>
                    <g data-id="Group-149" transform="translate(109.761388, 280.000000)">
                        <polygon data-id="Fill-138" fill="#F75275" points="0.620650759 50 60.4904989 50 60.4904989 0 0.620650759 0"></polygon>
                        <polygon data-id="Stroke-140" stroke="#FFFFFF" stroke-width="6" points="0.620650759 50 60.4904989 50 60.4904989 0 0.620650759 0"></polygon>
                        <polygon data-id="Fill-141" fill="#F75275" points="60.4904989 50 120.360347 50 120.360347 0 60.4904989 0"></polygon>
                        <polygon data-id="Stroke-142" stroke="#FFFFFF" stroke-width="6" points="60.4904989 50 120.360347 50 120.360347 0 60.4904989 0"></polygon>
                        <polygon data-id="Fill-143" fill="#F75275" points="120.360347 50 180.230195 50 180.230195 0 120.360347 0"></polygon>
                        <polygon data-id="Stroke-144" stroke="#FFFFFF" stroke-width="6" points="120.360347 50 180.230195 50 180.230195 0 120.360347 0"></polygon>
                        <polygon data-id="Fill-145" fill="#F75275" points="180.230195 50 240.100043 50 240.100043 0 180.230195 0"></polygon>
                        <polygon data-id="Stroke-146" stroke="#FFFFFF" stroke-width="6" points="180.230195 50 240.100043 50 240.100043 0 180.230195 0"></polygon>
                        <polygon data-id="Fill-147" fill="#F75275" points="240.100043 50 299.969892 50 299.969892 0 240.100043 0"></polygon>
                        <polygon data-id="Stroke-148" stroke="#FFFFFF" stroke-width="6" points="240.100043 50 299.969892 50 299.969892 0 240.100043 0"></polygon>
                    </g>
                    <text data-id="OS" font-size="18"  fill="#30079F">
                        <tspan x="128.903375" y="311.749464">OS</tspan>
                    </text>
                    <text data-id="OS" font-size="18"  fill="#30079F">
                        <tspan x="69.0395141" y="311.749464">OS</tspan>
                    </text>
                    <text data-id="OS" font-size="18"  fill="#30079F">
                        <tspan x="188.785197" y="311.749464">OS</tspan>
                    </text>
                    <text data-id="OS" font-size="18"  fill="#30079F">
                        <tspan x="248.649059" y="311.749464">OS</tspan>
                    </text>
                    <text data-id="OS" font-size="18"  fill="#30079F">
                        <tspan x="308.51292" y="311.749464">OS</tspan>
                    </text>
                    <text data-id="…" font-size="23"  fill="#30079F">
                        <tspan x="368.654178" y="307.249453">…</tspan>
                    </text>
                    <g data-id="Group-153" transform="translate(49.891540, 170.000000)">
                        <polygon data-id="Fill-150" fill="#F75275" points="0.620650759 110 359.83974 110 359.83974 0 0.620650759 0"></polygon>
                        <polygon data-id="Stroke-152" stroke="#FFFFFF" stroke-width="6" points="0.620650759 110 359.83974 110 359.83974 0 0.620650759 0"></polygon>
                    </g>
                    <text data-id="Cloud-Management-Platform" font-size="18"  fill="#30079F">
                        <tspan x="119.224217" y="207.067864">Cloud Management Platfo</tspan>
                        <tspan x="320.986217" y="207.067864" letter-spacing="0.144">r</tspan>
                        <tspan x="327.880217" y="207.067864">m</tspan>
                    </text>
                    <path d="M205.553145,245 C205.553145,253.25 198.817787,260 190.585683,260 L100.780911,260 C92.5488069,260 85.813449,253.25 85.813449,245 C85.813449,236.75 92.5488069,230 100.780911,230 L190.585683,230 C198.817787,230 205.553145,236.75 205.553145,245" data-id="Fill-154" fill="#FFFFFF" opacity="0.5"></path>
                    <text data-id="Automation" font-size="14"  fill="#30079F">
                        <tspan x="111.75705" y="250">Automation</tspan>
                    </text>
                    <path d="M375.184382,245 C375.184382,253.25 368.449024,260 360.21692,260 L270.412148,260 C262.180043,260 255.444685,253.25 255.444685,245 C255.444685,236.75 262.180043,230 270.412148,230 L360.21692,230 C368.449024,230 375.184382,236.75 375.184382,245" data-id="Fill-156" fill="#FFFFFF" opacity="0.5"></path>
                    <text data-id="Orchestration" font-size="14"  fill="#30079F">
                        <tspan x="276.399132" y="250">Orchestration</tspan>
                    </text>
                    <polygon data-id="Fill-158" fill="url(#linearGradient-2)" points="0.620650759 429.999 50.5121909 429.999 50.5121909 119.999 0.620650759 119.999"></polygon>
                    <polygon data-id="Stroke-160" stroke="#FFFFFF" stroke-width="6" stroke-linecap="square" points="0.620650759 430 50.5121909 430 50.5121909 120 0.620650759 120"></polygon>
                    <text data-id="Security" transform="translate(25.943601, 276.800004) rotate(-90.000000) translate(-25.943601, -276.800004) " font-size="18"  fill="#30079F">
                        <tspan x="-4.98915401" y="281">Security</tspan>
                    </text>
                    <polygon data-id="Fill-162" fill="url(#linearGradient-2)" points="410.10846 430 460 430 460 120 410.10846 120"></polygon>
                    <polygon data-id="Stroke-164" stroke="#FFFFFF" stroke-width="6" stroke-linecap="square" points="409.73128 430 459.62282 430 459.62282 120 409.73128 120"></polygon>
                    <text data-id="Beheer" transform="translate(435.054230, 274.800004) rotate(-90.000000) translate(-435.054230, -274.800004) " font-size="18"  fill="#30079F">
                        <tspan x="408.112798" y="279">Beheer</tspan>
                    </text>
                    <text data-id="Gebruikers" font-size="18"  fill="#FFFFFF">
                        <tspan x="187.467115" y="40.4013676">Geb</tspan>
                        <tspan x="219.957115" y="40.4013676" letter-spacing="0.32">r</tspan>
                        <tspan x="227.243115" y="40.4013676">uikers</tspan>
                    </text>
                    <g data-id="Page-1" transform="translate(27.000000, 61.000000)" stroke="#FFFFFF" stroke-width="3" stroke-linecap="round" stroke-linejoin="round">
                        <path d="M18.4605,0.641 C12.0625,0.641 6.8765,4.694 6.8765,13.359 C6.8765,16.614 5.5435,17.775 3.5435,18.441" data-id="Stroke-1"></path>
                        <path d="M25.4693,22.5692 C29.2603,22.5692 32.0483,24.1152 33.6573,25.7652 C36.5313,28.7132 36.4103,33.3412 36.4103,38.1882 L36.4103,46.7242" data-id="Stroke-3"></path>
                        <path d="M9.3501,20.5155 C11.4711,23.2125 14.7641,24.9435 18.4601,24.9435" data-id="Stroke-5"></path>
                        <path d="M14.6094,6.1591 C18.2764,4.5761 22.4984,5.6231 24.6194,6.8451 C27.3644,8.4271 30.0454,11.3051 30.0454,16.1941" data-id="Stroke-7"></path>
                        <path d="M27.5708,20.5155 C25.4498,23.2125 22.1578,24.9435 18.4608,24.9435" data-id="Stroke-9"></path>
                        <path d="M5.2354,24.6581 C0.0514,27.4651 0.5104,32.3171 0.5104,38.1881 L0.5104,46.7241" data-id="Stroke-11"></path>
                        <path d="M18.4605,0.641 C20.8145,0.641 23.3265,1.041 25.2035,2.17" data-id="Stroke-13"></path>
                        <path d="M90.6763,13.3592 C90.6763,19.7572 85.4893,24.9432 79.0923,24.9432 C72.6943,24.9432 67.5073,19.7572 67.5073,13.3592 C67.5073,6.9612 72.6943,1.7752 79.0923,1.7752 C85.4893,1.7752 90.6763,6.9612 90.6763,13.3592 Z" data-id="Stroke-15"></path>
                        <path d="M86.1006,22.5692 C89.8906,22.5692 92.6786,24.1152 94.2876,25.7652 C97.1616,28.7132 97.0416,33.3412 97.0416,38.1882 L97.0416,46.7242" data-id="Stroke-17"></path>
                        <path d="M65.8667,24.6581 C60.6817,27.4651 61.1417,32.3171 61.1417,38.1881 L61.1417,46.7241" data-id="Stroke-19"></path>
                        <path d="M388.7945,1.7748 C382.3965,1.7748 377.2105,6.9618 377.2105,13.3588 C377.2105,16.6138 375.8775,17.7748 373.8775,18.4418" data-id="Stroke-21"></path>
                        <path d="M379.6841,20.5155 C381.8051,23.2125 385.0981,24.9435 388.7941,24.9435" data-id="Stroke-23"></path>
                        <path d="M388.7945,1.7748 C395.1925,1.7748 400.3795,6.9618 400.3795,13.3588 C400.3795,16.6138 401.7115,17.7748 403.7115,18.4418" data-id="Stroke-25"></path>
                        <path d="M397.9048,20.5155 C395.7838,23.2125 392.4918,24.9435 388.7948,24.9435" data-id="Stroke-27"></path>
                        <path d="M395.8033,22.5692 C399.5943,22.5692 402.3823,24.1152 403.9913,25.7652 C406.8653,28.7132 406.7443,33.3412 406.7443,38.1882 L406.7443,46.7242" data-id="Stroke-29"></path>
                        <path d="M375.5694,24.6581 C370.3854,27.4651 370.8444,32.3171 370.8444,38.1881 L370.8444,46.7241" data-id="Stroke-31"></path>
                        <path d="M197.6626,9.7167 C206.9126,10.5917 210.1066,3.7537 210.1066,3.7537 C213.1886,5.8347 215.2146,9.3607 215.2146,13.3597 C215.2146,19.7577 210.0276,24.9437 203.6296,24.9437 C197.2316,24.9437 192.0456,19.7577 192.0456,13.3597 C192.0456,6.9617 197.2316,1.7747 203.6296,1.7747 C204.9706,1.7747 205.9956,2.2497 206.4326,0.4997" data-id="Stroke-33"></path>
                        <path d="M210.6387,22.5692 C214.4297,22.5692 217.2177,24.1152 218.8267,25.7652 C221.7007,28.7132 221.5797,33.3412 221.5797,38.1882 L221.5797,46.7242" data-id="Stroke-35"></path>
                        <path d="M190.4048,24.6581 C185.2198,27.4651 185.6798,32.3171 185.6798,38.1881 L185.6798,46.7241" data-id="Stroke-37"></path>
                        <path d="M332.2613,3.7533 C335.3433,5.8353 337.3693,9.3603 337.3693,13.3593 C337.3693,19.7573 332.1823,24.9433 325.7843,24.9433 C319.3863,24.9433 314.1993,19.7573 314.1993,13.3593 C314.1993,6.9613 319.3863,1.7743 325.7843,1.7743 C327.1253,1.7743 328.1503,2.2503 328.5873,0.5003" data-id="Stroke-39"></path>
                        <path d="M332.7935,22.5692 C336.5835,22.5692 339.3715,24.1152 340.9805,25.7652 C343.8545,28.7132 343.7345,33.3412 343.7345,38.1882 L343.7345,46.7242" data-id="Stroke-41"></path>
                        <path d="M312.5591,24.6581 C307.3751,27.4651 307.8341,32.3171 307.8341,38.1881 L307.8341,46.7241" data-id="Stroke-43"></path>
                        <path d="M275.8506,13.3592 C275.8506,19.7572 270.6636,24.9432 264.2666,24.9432 C257.8686,24.9432 252.6816,19.7572 252.6816,13.3592 C252.6816,6.9612 257.8686,1.7752 264.2666,1.7752 C270.6636,1.7752 275.8506,6.9612 275.8506,13.3592 Z" data-id="Stroke-45"></path>
                        <path d="M271.2749,22.5692 C275.0649,22.5692 277.8529,24.1152 279.4619,25.7652 C282.3359,28.7132 282.2159,33.3412 282.2159,38.1882 L282.2159,46.7242" data-id="Stroke-47"></path>
                        <path d="M251.0406,24.6581 C245.8566,27.4651 246.3156,32.3171 246.3156,38.1881 L246.3156,46.7241" data-id="Stroke-49"></path>
                        <path d="M129.0313,13.3592 C129.0313,2.9612 128.8843,1.7752 140.6153,1.7752 C152.3473,1.7752 152.2003,2.9612 152.2003,13.3592 C152.2003,19.7572 147.0133,24.9432 140.6153,24.9432 C134.2183,24.9432 129.0313,19.7572 129.0313,13.3592" data-id="Stroke-51"></path>
                        <path d="M147.6246,22.5692 C151.4146,22.5692 154.2026,24.1152 155.8116,25.7652 C158.6856,28.7132 158.5656,33.3412 158.5656,38.1882 L158.5656,46.7242" data-id="Stroke-53"></path>
                        <path d="M127.3907,24.6581 C122.2057,27.4651 122.6657,32.3171 122.6657,38.1881 L122.6657,46.7241" data-id="Stroke-55"></path>
                        <path d="M85.1871,11.557 C80.1861,12.792 76.7361,10.262 75.1521,8.144 C75.1521,8.144 72.4361,10.125 68.7691,10.542" data-id="Stroke-57"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
