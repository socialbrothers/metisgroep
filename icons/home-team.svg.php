<svg width="2380px" height="600px" viewBox="0 0 2380 600" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
    <title>team</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g data-id="Metis" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g data-id="team">
            <g data-id="Team">
                <polygon data-id="Fill-2-Copy-2" fill="#FF707D" points="0 600 2380 600 2380 300 0 300"></polygon>
                <polygon data-id="Fill-2-Copy-3" fill="#CDCDC8" points="0 300 2380 300 2380 0 0 0"></polygon>
                <polygon data-id="Triangle-Copy" fill="#CE437D" points="2080 300 2380 600 1780 600"></polygon>
                <polygon data-id="Triangle-Copy-2" fill="#F65275" points="300 300 600 600 0 600"></polygon>
                <polygon data-id="Fill-11" points="1480 600 1780 600 1780 300"></polygon>
                <polygon data-id="Fill-15" fill="#F65275" points="2080 300 1780 300 1930 450"></polygon>
            </g>
        </g>
    </g>
</svg>
