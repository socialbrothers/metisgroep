<svg width="1160px" height="580px" viewBox="0 0 1160 580" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 44.1 (41455) - http://www.bohemiancoding.com/sketch -->
    <title>Haas</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g data-id="Metis" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g data-id="Haas" fill-rule="nonzero">
            <polygon data-id="Shape" fill="#CE437D" points="860.7 239.7 789 168 717.3 96.3 645.5 168 789 168 717.3 239.7 789 311.5"></polygon>
            <polygon data-id="Shape" fill="#CE437D" points="717.3 239.7 430.4 239.7 371 180.3 371 311.5 299.2 383.2 371 454.9 371 383.2 573.8 383.2 573.8 383.2 615.8 383.2 717.3 484.6 818.7 484.6 717.3 383.2 717.3 383.2 717.3 239.7"></polygon>
            <polygon data-id="Shape" fill="#A73486" points="573.8 383.2 573.8 383.2 717.3 239.7 430.4 239.7"></polygon>
            <polygon data-id="Shape" fill="#CE437D" points="371 383.2 371 383.2 573.8 383.2 371 180.3"></polygon>
            <polygon data-id="Shape" fill="#F65275" points="371 311.5 299.2 383.2 371 454.9"></polygon>
            <polygon data-id="Shape" fill="#7F268E" points="717.3 96.3 645.5 168 789 168"></polygon>
            <polygon data-id="Shape" fill="#CE437D" points="717.3 484.6 717.3 484.6 818.7 484.6 717.3 383.2 615.8 383.2"></polygon>
            <rect data-id="Rectangle-path" fill="#30089F" transform="translate(789.007781, 239.693280) rotate(-45.000000) translate(-789.007781, -239.693280) " x="738.307781" y="188.97328" width="101.4" height="101.44"></rect>
            <polygon data-id="Shape" fill="#581797" points="573.8 383.2 717.3 383.2 717.3 239.7"></polygon>
        </g>
    </g>
</svg>
