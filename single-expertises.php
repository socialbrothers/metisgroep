<?php get_header();

      if (have_posts()) :
        while (have_posts()) : the_post();

          get_template_part('partials/single-post/expertise/hero');

          get_template_part('partials/single-post/expertise/content');

        endwhile;
      endif;

      get_template_part('partials/over-network/footer');

      get_footer(); ?>
