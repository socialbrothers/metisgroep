<?php

if (!defined('ABSPATH')) {
    exit('Forbidden');
}

if (function_exists('acf_add_local_field_group')) :

    acf_add_local_field_group(array(
        'key' => 'group_59412b430c727',
        'title' => 'Post',
        'fields' => array(
            array(
                'key' => 'field_59412bf090238',
                'label' => 'Featured image',
                'name' => 'mit_featured',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'button_label' => 'Nieuwe afbeelding',
                'min' => 0,
                'max' => 1,
                'layouts' => array(
                    array(
                        'key' => '59412bf6d3429',
                        'name' => 'mit_featured_image',
                        'label' => 'Afbeelding',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_59412c2390239',
                                'label' => 'Afbeelding',
                                'name' => 'mit_featured_image_src',
                                'type' => 'image',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'return_format' => 'array',
                                'preview_size' => 'thumbnail',
                                'library' => 'all',
                                'min_width' => '',
                                'min_height' => '',
                                'min_size' => '',
                                'max_width' => '',
                                'max_height' => '',
                                'max_size' => '',
                                'mime_types' => '',
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    array(
                        'key' => '59412c449023a',
                        'name' => 'mit_featured_tangram',
                        'label' => 'Tangram',
                        'display' => 'row',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_59412c649023b',
                                'label' => 'Tangram dier',
                                'name' => 'mit_featured_tangram_sort',
                                'type' => 'select',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'choices' => array(
                                    'Haas' => 'Haas',
                                    'Hart' => 'Hart',
                                    'Hond' => 'Hond',
                                    'Karper' => 'Karper',
                                    'Muis' => 'Muis',
                                    'Paard' => 'Paard',
                                    'Vis' => 'Vis',
                                    'Vliegtuig' => 'Vliegtuig',
                                    'Vogel' => 'Vogel',
                                    'Vos' => 'Vos',
                                ),
                                'default_value' => 'Haas',
                                'allow_null' => 0,
                                'multiple' => 0,
                                'ui' => 0,
                                'ajax' => 0,
                                'placeholder' => '',
                                'disabled' => 0,
                                'readonly' => 0,
                                'return_format' => 'value',
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                ),
            ),
            array(
                'key' => 'field_59412cd266b32',
                'label' => 'Show on single page as header',
                'name' => 'mit_featured_show_on_single',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
                'ui' => 0,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
        'show_in_rest' => false,
    ));

endif;
