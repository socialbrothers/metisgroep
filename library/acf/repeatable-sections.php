<?php

if (!defined('ABSPATH')) {
    exit('Forbidden');
}

if (function_exists('acf_add_local_field_group')) :

    acf_add_local_field_group(array(
        'key' => 'group_5a7ac2329caaa',
        'title' => 'Repeatable Sections',
        'fields' => array(
            array(
                'key' => 'field_5a7ac23bc4136',
                'label' => 'Repeatable Sections',
                'name' => 'mit_rs',
                'type' => 'repeater',
                'instructions' => 'Hier kun je secties herhalen, als je een nieuwe sectie aan wil maken of bewerken kan dit onder het menu item "Secties".',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 0,
                'max' => 0,
                'layout' => 'table',
                'button_label' => 'Sectie toevoegen',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5a7ac270c4137',
                        'label' => 'Section',
                        'name' => 'mit_rs_section',
                        'type' => 'post_object',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'post_type' => array(
                            0 => 'secties',
                        ),
                        'taxonomy' => array(),
                        'allow_null' => 0,
                        'multiple' => 0,
                        'return_format' => 'object',
                        'ui' => 1,
                    ),
                ),
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                ),
                array(
                    'param' => 'page_template',
                    'operator' => '!=',
                    'value' => 'page-network.php',
                ),
                array(
                    'param' => 'page',
                    'operator' => '!=',
                    'value' => '2',
                ),
                array(
                    'param' => 'page_template',
                    'operator' => '!=',
                    'value' => 'page-faq.php',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
        'show_in_rest' => false,
    ));

endif;
