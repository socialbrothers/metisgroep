<?php

if (!defined('ABSPATH')) {
    exit('Forbidden');
}

if (function_exists('acf_add_local_field_group')) :

    acf_add_local_field_group(array(
        'key' => 'group_5a7ac1104c153',
        'title' => 'Repeatable Sections Types',
        'fields' => array(
            array(
                'key' => 'field_5a7ac134a9bd9',
                'label' => 'Sectie Type',
                'name' => 'mit_rs_section_type',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'layouts' => array(
                    '5a7ac143bda98' => array(
                        'key' => '5a7ac143bda98',
                        'name' => 'mit_rs_section_type_text',
                        'label' => 'Tekst',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5a7ac1f123ff5',
                                'label' => 'Body',
                                'name' => 'mit_rs_section_type_text_body',
                                'type' => 'wysiwyg',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'tabs' => 'all',
                                'toolbar' => 'full',
                                'media_upload' => 1,
                                'delay' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    '5a7b0939c491c' => array(
                        'key' => '5a7b0939c491c',
                        'name' => 'mit_rs_section_type_vacancy',
                        'label' => 'Vacatures',
                        'display' => 'block',
                        'sub_fields' => array(),
                        'min' => '',
                        'max' => '',
                    ),
                    '5a7b1eb6d3280' => array(
                        'key' => '5a7b1eb6d3280',
                        'name' => 'mit_rs_section_type_quote_worker',
                        'label' => 'Quotes van medewerkers',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5a842b3eae4a5',
                                'label' => 'Quote medewerker',
                                'name' => 'mit_rs_section_type_quote_worker_id',
                                'type' => 'post_object',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'post_type' => array(
                                    0 => 'medewerkers',
                                ),
                                'taxonomy' => array(),
                                'allow_null' => 0,
                                'multiple' => 0,
                                'return_format' => 'id',
                                'ui' => 1,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    '5a7b16a02cbf6' => array(
                        'key' => '5a7b16a02cbf6',
                        'name' => 'mit_rs_section_type_puzzle',
                        'label' => 'Puzzel',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5a7b16b42cbf7',
                                'label' => 'Body bovenkant',
                                'name' => 'mit_rs_section_type_puzzle_body_top',
                                'type' => 'wysiwyg',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'tabs' => 'all',
                                'toolbar' => 'full',
                                'media_upload' => 1,
                                'delay' => 0,
                            ),
                            array(
                                'key' => 'field_5a7b16e42cbf8',
                                'label' => 'Body onderkant',
                                'name' => 'mit_rs_section_type_puzzle_body_bottom',
                                'type' => 'wysiwyg',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'tabs' => 'all',
                                'toolbar' => 'full',
                                'media_upload' => 1,
                                'delay' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    '5a84315802ae9' => array(
                        'key' => '5a84315802ae9',
                        'name' => 'mit_rs_section_type_slider_yearly_budget',
                        'label' => 'Jaarbudget slider',
                        'display' => 'block',
                        'sub_fields' => array(),
                        'min' => '',
                        'max' => '',
                    ),
                ),
                'button_label' => 'Sectie type',
                'min' => '',
                'max' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'secties',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
        'show_in_rest' => false,
    ));

endif;
