<?php

  function shortcode_paragraph_handler ( $atts, $content = null ) {
    $a = shortcode_atts( array(
      'class' => '',
	  ), $atts );

    return '<p class="e-paragraph ' . esc_attr($a['class']) . '">' . $content . '</p>';
  }

  add_shortcode('paragraph', 'shortcode_paragraph_handler');

?>
