<?php

  function shortcode_button_handler ( $atts, $content = null ) {
    $a = shortcode_atts( array(
      'class' => 'e-button--blue',
      'href' => '',
	  ), $atts );

    return '<a href="' . esc_attr($a['href']) . '" class="e-button ' . esc_attr($a['class']) . '" title="' . $content . '"><span>' . $content . '</span></a>';
  }

  add_shortcode('button', 'shortcode_button_handler');

?>
