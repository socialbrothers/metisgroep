<?php get_header(); ?>

	<?php if (have_posts()) : ?>

		<?php get_template_part('partials/partners/intro'); ?>

		<?php get_template_part('partials/partners/highlighted'); ?>

		<?php get_template_part('partials/partners/logo-section'); ?>

	<?php endif; ?>

<?php get_footer(); ?>
